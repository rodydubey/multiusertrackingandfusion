#import sys
#sys.path.append("../../i2p/tools/py")

#thread
import threading

from numpy import array

class WorldEntity:
    def __init__(self, name):
        self.lock = threading.Lock()
        self.name = name
        self.position = array([0.0,0.0,0.0])
        self.orientation = array([1.0,0.0,0.0,0.0])
        
    def setPosition(self, position):
        self.lock.acquire()
        self.position = array(position)
        self.lock.release()
        
    def getPosition(self):
        self.lock.acquire()
        position = array(self.position)
        self.lock.release()
        return position
        
    def setOrientation(self, orientation):
        self.lock.acquire()
        self.orientation = array(orientation)
        self.lock.release()
        
    def getOrientation(self):
        self.lock.acquire()
        orientation = array(self.orientation)
        self.lock.release()
        return orientation