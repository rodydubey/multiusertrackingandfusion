#ifndef __USERDETAIL_H_
#define __USERDETAIL_H_

#define _WINSOCKAPI_ 
#include <Windows.h>

#include "UserTrackingService.h"

class UserDetail
{
    public:
        static enum UserStatus {NEW,ENGAGED,NONENGAGED,LOST};

        UserDetail();
        virtual ~UserDetail();

        int getId();
        void setId(int id);

		bool getSpeaking();
		void setSpeaking(bool flag);

		std::string getCurrentGesture();
		void setCurrentGesture(std::string gestureName);

		int getGender();
		void setGender(int gender); // 1 for male 0 for female

		double getTimeSpent();
		void setTimeSpent(double time);

        std::string& getName();
        void setName(std::string &name);

        UserStatus getStatus();
        std::string getStatusString();
        void setStatus(UserStatus status);

        bool isVisible();
        void setVisibility(bool visibility);
                
        static std::string statusToString(UserStatus status);
                
        imi::Skeleton m_positions;


    private:
        int m_id;
        UserStatus m_status;
        bool m_visible;
        static std::string m_nameStatus[];
        std::string m_name;

		bool _speaking;
		std::string _currentGesture;
		int _gender;
		double _timeSpent;
};

#endif