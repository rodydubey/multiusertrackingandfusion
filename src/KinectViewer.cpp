#include "PoseEstimator.h"

int main()
{
	PoseEstimator* m_pTester = new PoseEstimator();
	if(m_pTester->Init())
	{
		m_pTester->ViewVideo();
		m_pTester->Clear();
	}
	return 0;
}

