#ifndef _USERCOUNT_H_
#define _USERCOUNT_H_

#include "UserDetail.h"
#include <vector>
#include "WorldFusionService.h"

class UserCount
{
    public:

        UserCount();
        virtual ~UserCount();

        int getNbUsers();
        UserDetail* getUserFromId(int idUser);
        UserDetail* getUserFromIndex(int index);
        void addUser(int idUser);
        void eraseUser(int idUser);

        std::vector<UserDetail*> m_users;
		std::vector<imi::userDetail> _details;
};

#endif