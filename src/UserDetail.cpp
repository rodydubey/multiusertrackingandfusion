#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include "UserDetail.h"

std::string UserDetail::m_nameStatus[] = {std::string("New"),
                                        std::string("Engaged"),
                                        std::string("NonEngaged"),
                                        std::string("Calibrated"),
                                        std::string("Lost")};

UserDetail::UserDetail()
{
    m_id = -1;
    m_status = UserDetail::NEW;
    m_visible = false;
    m_name = std::string("unknown");
	_speaking = false;
	_currentGesture = "none";
	_gender = -9;
	_timeSpent = 0.0;

}

UserDetail::~UserDetail()
{
}

int UserDetail::getId()
{
    return m_id;
}

void UserDetail::setId(int id)
{
    m_id = id;
}

std::string &UserDetail::getName()
{
    return m_name;
}

void UserDetail::setName(std::string& name)
{
    m_name = name;
}

UserDetail::UserStatus UserDetail::getStatus()
{
    return m_status;
}

std::string UserDetail::getStatusString()
{
    return statusToString(m_status);
}

void UserDetail::setStatus(UserStatus status)
{
    m_status = status;
}

bool UserDetail::isVisible()
{
    return m_visible;
}

void UserDetail::setVisibility(bool visibility)
{
    m_visible = visibility;
}

std::string UserDetail::statusToString(UserStatus status)
{
    if(status < NEW || status > LOST)
        return std::string("");
    else
        return m_nameStatus[status];
}

bool UserDetail::getSpeaking()
{
	return _speaking;
}
void UserDetail::setSpeaking(bool flag)
{
	_speaking = flag;
}

std::string UserDetail::getCurrentGesture()
{
	return _currentGesture;
}
void UserDetail::setCurrentGesture(std::string gestureName)
{
	_currentGesture = gestureName;
}

int UserDetail::getGender()
{
	return _gender;
}
void UserDetail::setGender(int gender) // 1 for male 0 for female
{
	_gender = gender;
}

double UserDetail::getTimeSpent()
{
	return _timeSpent;
}
void UserDetail::setTimeSpent(double time)
{
	_timeSpent = time;
}
