// VidCapDlg.cpp : implementation file
//

//#include "vld.h"

// This file has been tempered badly. clickstartbutton function is called from the constructor to avoid the GUI.
// This is done to hide MFC windows. This is done to make this project run independently. We will copy the image in the executable folder.
// Make sure that image name is naocapture.jpg. It is hardcoded in phython script from the nao side. 
// It is not super fantastic, but works!
// brought to you by: Rohit, and shantanu

// #include "VidCapNao.h"
#include "VidCapDlgNao.h"

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>

#include <fstream>
#define MAXMESSAGELENGTH 1000
#define NUM_OF_PATCH 7
#define LBP_P 8
#define LBP_R 2
#define NUM_OF_USER_TEMPLATE 10

#define haarcascade_face "D:\\src\\i2p_face_recognition\\build\\i2p_face_recognition_data\\Haarcascades\\haarcascade_frontalface_alt2.xml"
#define haarcascade_eye_l "D:\\src\\i2p_face_recognition\\build\\i2p_face_recognition_data\\Haarcascades\\haarcascade_mcs_lefteye.xml"
#define haarcascade_eye_r "D:\\src\\i2p_face_recognition\\build\\i2p_face_recognition_data\\Haarcascades\\haarcascade_mcs_righteye.xml"

#define tempChar "<face_recognition>"
#define tempChar2 "</face_recognition>"
#define NEWUSERNAME1 L"NewUser1"

#define tempChar "<face_recognition>"
#define tempChar2 "</face_recognition>"
const char* widecstr;
std::string widestr;
using namespace cv;
std::string name;

// CVidCapDlg dialog
CVidCapDlg::CVidCapDlg(std::string devicename)
	: m_bIsRunning(FALSE)
    , m_nTimer(0)
{	
    m_devicename = devicename;
    m_ellipse = NULL;
    m_matrix_verify = NULL;
    for (int i = 0; i<NUM_OF_REGISTER_USER;i++)
	    m_WatchList[i].verify_face = NULL;
        
	for (int i = 0;i<2;i++)
        m_TrackList[i].face_rect = NULL;
    cascade_face = cvLoadHaarClassifierCascade(haarcascade_face,cvSize(20,20));
	if (cascade_face == NULL)
        std::cerr << "failed to load face detector\n";

	cascade_eye_l = cvLoadHaarClassifierCascade(haarcascade_eye_l,cvSize(18,12));
	cascade_eye_r = cvLoadHaarClassifierCascade(haarcascade_eye_r,cvSize(18,12));
	if (cascade_eye_l == NULL || cascade_eye_r  == NULL)
        std::cerr << "failed to load eye related data\n";

	for (int i = 0; i<NUM_OF_REGISTER_USER;i++)
	{
		int histSize = 256*NUM_OF_PATCH*NUM_OF_PATCH*NUM_OF_USER_TEMPLATE;
		//read in verify_face
		m_WatchList[i].verify_face = new double[histSize];
		char FileName[300];
		sprintf(FileName,"D:\\src\\i2p_face_recognition\\build\\i2p_face_recognition_data\\%d",i);
		//sprintf(FileName,"c:/FaceRecognitionDemoData_new/%d",i);
//		sprintf(FileName,"D://src//i2p_face_recognition//build//i2p_face_recognition_data//%d",i);
		FILE* fid = fopen(FileName,"rb");		
		if (fid==NULL)
		{
			 cout << "Not able to open file\n!";			
		}
		fread(m_WatchList[i].verify_face,sizeof(double),histSize,fid);
		fclose(fid);

		//read in user name;
		m_WatchList[i].user_name = "";
	}
		
	m_WatchList[0].user_name = "";
	m_WatchList[1].user_name = "Jianfeng";
	m_WatchList[2].user_name = "Flavien";
	m_WatchList[3].user_name = "Nadia";
	m_WatchList[4].user_name = "Bertil";
	m_WatchList[5].user_name = "Rohit";
	m_WatchList[6].user_name = "Zerrin";
	m_WatchList[7].user_name = "Bingbing";
	m_WatchList[8].user_name = "Fuchang";
	m_WatchList[9].user_name = "Zerrin";

	for (int i = 0;i<2;i++)
	{
        //if(m_TrackList[i].face_rect!=NULL)
           // delete m_TrackList[i].face_rect;
		m_TrackList[i].face_rect = new CvRect;
		m_TrackList[i].user_name = "";
	}

	ResetTrackList();
	FVCount = 0;

   }

CVidCapDlg::~CVidCapDlg()
{
	if (m_ellipse != NULL)        
		delete [] m_ellipse;
        
    for (int i = 0; i<NUM_OF_REGISTER_USER;i++)
	    delete [] m_WatchList[i].verify_face;
	for (int i = 0;i<2;i++)
        delete m_TrackList[i].face_rect;
	
    if (m_matrix_verify!=NULL)
    	delete [] m_matrix_verify;
}

// void CVidCapDlg::init(string imgName);
std::string CVidCapDlg::init(cv::Mat nao)
{
    // TODO: Add your message handler code here and/or call default
	 FVCount++;
	if (UCount == 0)
		FVCount = 10000;

    if (m_bIsRunning = TRUE) 
        {
            CvSize minSize,maxSize;
            CvRect ROI;
						
            int scale = 2;
            // Tic();        
        
            cv::Mat tempMat;
            // cv::Mat nao = cv::imread(imgName);
            tempMat = nao;       
            
            /*cv::Mat tempMat = cv::Mat(ImgHeight,ImgWidth,CV_8UC3,(void*)pData);*/

            //cv::Mat resMat = cv::Mat(60,80,CV_8UC3);
            //cv::resize(tempMat,resMat,cv::Size(0,0),0.125,0.125);
            //imshow("lol",resMat);
			
            IplImage pImage = tempMat;

            IplImage* small_image1 = cvCreateImage( cvSize(int(pImage.width/scale),int(pImage.height/scale)), IPL_DEPTH_8U, 3);
            //IplImage* small_image2 = cvCreateImage( cvSize(int(pImage->width/4),int(pImage->height/4)), IPL_DEPTH_8U, 3);
            cvPyrDown(&pImage, small_image1, CV_GAUSSIAN_5x5 );
			
		
            //cvReleaseImage( &small_image1 );
			
            //UCount = 0;
            if (UCount == 0) // previously no user
			{
				ROI.x = 0;
				ROI.y = 0;
				ROI.width = 320;
				ROI.height = 240;

				cvSetImageROI(small_image1,ROI);

				minSize.height = std::max(20,int(50/scale));
				minSize.width = std::max(20,int(50/scale));
				maxSize.height = int(300/scale);
				maxSize.width = int(300/scale);

				CvSeq* faces;// = new CvSeq();
				CvMemStorage* storage = cvCreateMemStorage(0);
				faces = cvHaarDetectObjects(small_image1,cascade_face, storage,1.3,3, 0, minSize,maxSize );

				CvRect* faceR;
				UCount = faces->total;

				// If no user or too many user
				if (UCount == 0 || UCount > 2)
				{
					ResetTrackList();
					cvReleaseMemStorage(&storage);
				}
				else // 1 or 2 users
				{
					for (int i = 0; i < UCount; i++)
					{
						faceR = (CvRect*)cvGetSeqElem( faces, i );
						m_TrackList[i].face_rect->x = faceR->x;
						m_TrackList[i].face_rect->y = faceR->y;
						m_TrackList[i].face_rect->width = faceR->width;
						m_TrackList[i].face_rect->height = faceR->height;
					}
					cvReleaseMemStorage(&storage);

					for (int i = 0; i < UCount; i++) 
					{
                        cv::Rect rface(int(m_TrackList[i].face_rect->x*scale), int(m_TrackList[i].face_rect->y*scale), int(m_TrackList[i].face_rect->width*scale), int(m_TrackList[i].face_rect->height*scale));
						cv::rectangle(tempMat,rface,cv::Scalar(0,0,255),2);
										
						RECT *eyeRectL = new RECT;// = new CvRect();
						DetectBothEyeFromFaceImage(&pImage, small_image1, m_TrackList[i].face_rect,cascade_eye_l,scale,0, &eyeRectL);

						if (eyeRectL->left > 0)
                        {
                            cv::Rect rel(eyeRectL->left, eyeRectL->top, eyeRectL->right - eyeRectL->left + 1, eyeRectL->bottom - eyeRectL->top + 1);
						    cv::rectangle(tempMat,rel,cv::Scalar(0,0,255),2);
                        }

						RECT *eyeRectR = new RECT;// = new CvRect();
						DetectBothEyeFromFaceImage(&pImage, small_image1, m_TrackList[i].face_rect,cascade_eye_r,scale,1,&eyeRectR);

						if (eyeRectR->left > 0)
                        {
                            cv::Rect rel(eyeRectR->left, eyeRectR->top, eyeRectR->right - eyeRectR->left + 1, eyeRectR->bottom - eyeRectR->top + 1);
						    cv::rectangle(tempMat,rel,cv::Scalar(0,0,255),2);
						}

						// Do face verification
						if (FVCount*m_nTimerInterval>1000 && eyeRectL->left>0 && eyeRectR->left>0)
						{
							FVCount = 0;
							//wchar_t* UserName = L"";
							// create new image for the grayscale version
							cvResetImageROI(&pImage);
							IplImage *dst = cvCreateImage(cvGetSize(&pImage), IPL_DEPTH_8U, 1 );
							cvCvtColor( &pImage, dst, CV_RGB2GRAY );

							FaceVerification(dst, eyeRectL, eyeRectR, i/*&(m_TrackList[i].user_name*/);
							cvReleaseImage(&dst);
							//TrackList[i].user_name = UserName;
							//delete UserName;
						}
						delete [] eyeRectR;
						delete [] eyeRectL;
					}
				}
			}
			else
			{
				for (int i = 0;i<UCount;i++)
				{
					ROI.x = std::max(0,int(m_TrackList[i].face_rect->x - m_TrackList[i].face_rect->width*ScanNeighborhood));
					ROI.y = std::max(0,int(m_TrackList[i].face_rect->y - m_TrackList[i].face_rect->height*ScanNeighborhood));;
					ROI.width = std::min(320,int(m_TrackList[i].face_rect->width*(1.0f+2*ScanNeighborhood)));	
					ROI.height = std::min(240,int(m_TrackList[i].face_rect->height*(1.0f+2*ScanNeighborhood)));
					//ROI = CvRect();
					cvSetImageROI(small_image1,ROI);

					minSize.height = std::max(20,int(m_TrackList[i].face_rect->height*0.8f));
					minSize.width = std::max(20,int(m_TrackList[i].face_rect->width*0.8f));
					maxSize.height = std::min(int(300/scale),int(m_TrackList[i].face_rect->height*1.2f));
					maxSize.width = std::min(int(300/scale),int(m_TrackList[i].face_rect->width*1.2f));

					CvSeq* faces;// = new CvSeq();
					CvMemStorage* storage = cvCreateMemStorage(0);

					faces = cvHaarDetectObjects(small_image1,cascade_face, storage,1.3,3, 0, minSize,maxSize );
//					faces = cvHaarDetectObjects(small_image1,cascade_face, storage,1.1,3, CV_HAAR_FIND_BIGGEST_OBJECT, minSize,maxSize );
					int CC = faces->total;
					if (CC!=1)
					{
						cvReleaseMemStorage(&storage);
						ResetTrackList();
						continue;
					}

					CvRect* faceRectInROI = (CvRect*)cvGetSeqElem( faces, 0 );
					m_TrackList[i].face_rect->x = faceRectInROI->x + ROI.x;
					m_TrackList[i].face_rect->y = faceRectInROI->y + ROI.y;
					m_TrackList[i].face_rect->width = faceRectInROI->width;
					m_TrackList[i].face_rect->height = faceRectInROI->height;

					cvReleaseMemStorage(&storage);

					if (CC != 1)
					{
						ResetTrackList();	//restart
					}
					else
					{
                        cv::Rect rface(int(m_TrackList[i].face_rect->x*scale), int(m_TrackList[i].face_rect->y*scale), int(m_TrackList[i].face_rect->width*scale), int(m_TrackList[i].face_rect->height*scale));
						cv::rectangle(tempMat,rface,cv::Scalar(0,0,255),2);
										
						RECT *eyeRectL = new RECT;// = new CvRect();
						DetectBothEyeFromFaceImage(&pImage, small_image1, m_TrackList[i].face_rect,cascade_eye_l,scale,0, &eyeRectL);

						if (eyeRectL->left > 0)
                        {
                            cv::Rect rel(eyeRectL->left, eyeRectL->top, eyeRectL->right - eyeRectL->left + 1, eyeRectL->bottom - eyeRectL->top + 1);
						    cv::rectangle(tempMat,rel,cv::Scalar(0,0,255),2);
						}

						RECT *eyeRectR = new RECT;// = new CvRect();
						DetectBothEyeFromFaceImage(&pImage, small_image1, m_TrackList[i].face_rect,cascade_eye_r,scale,1,&eyeRectR);

						if (eyeRectR->left > 0)
                        {
                            cv::Rect rel(eyeRectR->left, eyeRectR->top, eyeRectR->right - eyeRectR->left + 1, eyeRectR->bottom - eyeRectR->top + 1);
						    cv::rectangle(tempMat,rel,cv::Scalar(0,0,255),2);
						}
								
						// Do face verification
						if (FVCount*m_nTimerInterval>1000 && eyeRectL->left>0 && eyeRectR->left>0)
						{
							FVCount = 0;
							//wchar_t* UserName = L"";
							// create new image for the grayscale version

							cvResetImageROI(&pImage);
							IplImage *dst = cvCreateImage(cvGetSize(&pImage), IPL_DEPTH_8U, 1 );
							cvCvtColor( &pImage, dst, CV_RGB2GRAY );

							FaceVerification(dst, eyeRectL, eyeRectR, i /*, &m_TrackList[i].user_name*/);
							cvReleaseImage(&dst);
							//TrackList[i].user_name = UserName;

							//delete [] UserName;
						}
						delete [] eyeRectL;
						delete [] eyeRectR;
					}
				}
			}
         	
            //if (UCount >= 1)
                //m_UserID.SetWindowTextW(m_TrackList[0].user_name);
	
                if (UCount>=1) 
                    //m_UserID2.SetWindowTextW(m_TrackList[1].user_name);

                    for (int i = 0;i<UCount;i++)
					{
                            IplImage* pCrownImg;
                            bool IsMale = false;

                            if (strcmp(m_TrackList[i].user_name,"Jianfeng")==0)
                                {
                                    std::cerr << "Jianfeng \n";
                                    pCrownImg = cvLoadImage("i2p_face_recognition_data/Jianfeng.bmp",1);
                                    cv::imwrite("Jianfeng.jpg",nao);
                                }
                            else if (strcmp(m_TrackList[i].user_name,"Nadia")==0)
                                {
                                    std::cerr << "Nadia \n";
                                    pCrownImg = cvLoadImage("i2p_face_recognition_data/Queens_Crown.bmp",1);
                                    cv::imwrite("Nadia.jpg",nao);
                                }
                            else if (strcmp(m_TrackList[i].user_name,"Junsong")==0)
                                {
                                    std::cerr << "Junsong\n";
                                    pCrownImg = cvLoadImage("i2p_face_recognition_data/Junsong.bmp",1);
                                    cv::imwrite("Junsong.jpg",nao);
                                }
                            else if (strcmp(m_TrackList[i].user_name,"Flavien")==0)
                                {
                                    std::cerr << "Flavien\n";
                                    pCrownImg = cvLoadImage("i2p_face_recognition_data/Flavien.bmp",1);
                                    cv::imwrite("Flavien.jpg",nao);
                                }
                            else if (strcmp(m_TrackList[i].user_name,"Hector")==0)
                                {
                                    std::cerr << "Hector\n";
                                    pCrownImg = cvLoadImage("i2p_face_recognition_data/Hector.bmp",1);
                                    cv::imwrite("Hector.jpg",nao);
                                }
                            else if (strcmp(m_TrackList[i].user_name,"Frank")==0)
                                {
                                    std::cerr << "Frank\n";
                                    pCrownImg = cvLoadImage("i2p_face_recognition_data/Frank.bmp",1);
                                    cv::imwrite("Frank.jpg",nao);
                                }
                            else if (strcmp(m_TrackList[i].user_name,"Bertil")==0)
                                {
                                    pCrownImg = cvLoadImage("i2p_face_recognition_data/Kings_Crown.bmp",1);
                                    cv::imwrite("Bertil.jpg",nao);
                                }
                            else if (strcmp(m_TrackList[i].user_name,"Daniel")==0)
                                {
                                    std::cerr << "Daniel\n";
                                    pCrownImg = cvLoadImage("i2p_face_recognition_data/Daniel.bmp",1);
                                    cv::imwrite("Daniel.jpg",nao);
                                }
							 else if (strcmp(m_TrackList[i].user_name,"Zerrin")==0)
                                {
                                    std::cerr << "Zerrin\n";
                                    pCrownImg = cvLoadImage("i2p_face_recognition_data/Zerrin.bmp",1);
                                    cv::imwrite("Zerrin.jpg",nao);
                                }
							 else if (strcmp(m_TrackList[i].user_name,"Rohit")==0)
                                {
                                    std::cerr << "Rohit\n";
                                    pCrownImg = cvLoadImage("i2p_face_recognition_data/Rohit.bmp",1);
                                    cv::imwrite("Rohit.jpg",nao);
                                }
							else
							{
								 /*char str[80];
								 char strc[100];
								 wcscpy (str,"i2p_face_recognition_data/");
								 wcscat (str,m_TrackList[i].user_name);
								 wcscat (str,L".bmp");
								 strcpy(strc,CT2CA(str));*/
								 pCrownImg = cvLoadImage("i2p_face_recognition_data/Zerrin.bmp",1);
							 }
                            if (pCrownImg!=0)
							{
								int image_height = pCrownImg->height;
								int image_width = pCrownImg->width;

								int win_x = int(scale*m_TrackList[i].face_rect->x);
								int win_y = int(scale*m_TrackList[i].face_rect->y);
								int win_width = int(scale*m_TrackList[i].face_rect->width);
								int win_height = int(scale*m_TrackList[i].face_rect->height);

								image_height = image_height*win_width/image_width;
								image_height = std::min(win_y,image_height);

								IplImage* pCrownImgResize = cvCreateImage(cvSize(win_width, image_height),pCrownImg->depth, pCrownImg->nChannels );
								cvResize(pCrownImg,pCrownImgResize,1);

								IplImage test = tempMat;
								cvSetImageROI(&test,cv::Rect(win_x,win_y-image_height,pCrownImgResize->width,pCrownImgResize->height));
								cvCopy(pCrownImgResize,&test);
								cvReleaseImage(&pCrownImg);
								cvReleaseImage(&pCrownImgResize);
							}
                        }

					static const char* lastusername = m_TrackList[0].user_name;
					static int count = 0;
					if (UCount > 0)
					{
						const char*currenUserName = m_TrackList[0].user_name;
						
						lastusername = currenUserName;
						name = lastusername;
			}
            
			if (FVCount*m_nTimerInterval>1000)
				ResetTrackList();
	        
                // m_dTicks += Toc();
                m_nFramesProcessed++;
            ResetTrackList();
            cvReleaseImage(&small_image1);
            cv::imshow("openCV",nao);
            // cv::waitKey(-1);
        }
	
	return name;
}
bool CVidCapDlg::ResetTrackList()
{
	UCount = 0;
	for (int i = 0;i<2;i++)
        {
            m_TrackList[i].face_rect->x = 0;
            m_TrackList[i].face_rect->y = 0;
            m_TrackList[i].face_rect->width = 0;
            m_TrackList[i].face_rect->height = 0;
            //		m_TrackList[i].user_name = "Unknown";
            m_TrackList[i].user_name = "";
        }
	return true;
}

bool CVidCapDlg::DetectBothEyeFromFaceImage(IplImage* pImage, IplImage* small_image1, CvRect* rr,CvHaarClassifierCascade* cascade_eye_l,int scale, int LR, RECT** eyeDetect)
{
	CvRect eyeRect;
	CvSize cvMin,cvMax;
	cvMin.height = std::max(12,int(rr->height/8));
	cvMin.width = std::max(18,int(rr->width/8));
	//cvMin.height = 12;
	//cvMin.width = 18;
	cvMax.height = std::max(12,int(rr->height/4));
	cvMax.width = std::max(18,int(rr->width/4));
			
	// detect left eyes
	if (rr->width>90)
        {
            if (LR == 0) //left eye
                eyeRect.x = rr->x;
            else // right eye
                eyeRect.x = rr->x+int(rr->width/2);
            eyeRect.y = rr->y + int(rr->height*0.2);
            eyeRect.width = int(rr->width/2);
            eyeRect.height = int(rr->height*0.6);

            cvSetImageROI(small_image1,eyeRect);

            DetectEyesFromFaceImage(small_image1,cascade_eye_l, cvMin, cvMax, eyeDetect);

            if ((*eyeDetect)->left > 0)
                {
                    CvSize cvMin2,cvMax2;								
                    cvMin2.width = int(cvMin.width*scale);
                    cvMin2.height = int(cvMin.height*scale);
                    cvMax2.width = int(cvMax.width*scale);
                    cvMax2.width = int(cvMax.height*scale);

                    eyeRect.x = int(((*eyeDetect)->left + eyeRect.x)*scale)-4;
                    eyeRect.y = int(((*eyeDetect)->top + eyeRect.y)*scale)-4;
                    eyeRect.width = std::min(int(((*eyeDetect)->right-(*eyeDetect)->left + 1) *scale)+8,ImgWidth-eyeRect.x);
                    eyeRect.height = std::min(int(((*eyeDetect)->bottom - (*eyeDetect)->top + 1)*scale)+8,ImgHeight-eyeRect.y);

                    cvSetImageROI(pImage,eyeRect);
                    DetectEyesFromFaceImage(pImage,cascade_eye_l, cvMin2, cvMax2,eyeDetect);

                    if ((*eyeDetect)->left > 0)
                        {
                            (*eyeDetect)->left = (*eyeDetect)->left + eyeRect.x;
                            (*eyeDetect)->top = (*eyeDetect)->top + eyeRect.y;
                            (*eyeDetect)->right = (*eyeDetect)->right + eyeRect.x;
                            (*eyeDetect)->bottom = (*eyeDetect)->bottom + eyeRect.y;
                        }
                }
        }
	else
        {
            // detect left eyes
            if (LR == 0)
                eyeRect.x = int(rr->x*scale);
            else
                eyeRect.x = int((rr->x+int(rr->width/2))*scale);
            eyeRect.y = int(rr->y*scale) + int(rr->height*0.2);
            eyeRect.width = int(rr->width/2*scale);
            eyeRect.height = int(rr->height*0.6*scale);

            cvSetImageROI(pImage,eyeRect);
            DetectEyesFromFaceImage(pImage,cascade_eye_l, cvSize(18, 12), cvSize(0,0),eyeDetect);

            if ((*eyeDetect)->left > 0)
                {
                    (*eyeDetect)->left = (*eyeDetect)->left + eyeRect.x;
                    (*eyeDetect)->top = (*eyeDetect)->top + eyeRect.y;
                    (*eyeDetect)->right = (*eyeDetect)->right + eyeRect.x;
                    (*eyeDetect)->bottom = (*eyeDetect)->bottom + eyeRect.y;
                }
        }

	return TRUE;
}

bool CVidCapDlg::DetectEyesFromFaceImage(IplImage* pImage,CvHaarClassifierCascade* cascade_eye, CvSize cvMin, CvSize cvMax, RECT** eyeRect)
{
	CvRect* rr;
	CvSeq* eye;// = new CvSeq();
	CvMemStorage* storage = cvCreateMemStorage(0);
	eye = cvHaarDetectObjects(pImage,cascade_eye, storage,1.1,3, CV_HAAR_FIND_BIGGEST_OBJECT, cvMin,cvMax );
	
	if (eye->total==1)
        {
            rr = (CvRect*)cvGetSeqElem(eye, 0);
            (*eyeRect)->left = rr->x;
            (*eyeRect)->top = rr->y;
            (*eyeRect)->right = rr->x + rr->width - 1;
            (*eyeRect)->bottom = rr->y + rr->height - 1;
            cvReleaseMemStorage(&storage);
            return TRUE;
        }
	else
        {
            (*eyeRect)->left = 0;
            (*eyeRect)->top = 0;
            (*eyeRect)->right = 0;
            (*eyeRect)->bottom = 0;
            cvReleaseMemStorage(&storage);
            return FALSE;
        }
}

// void CVidCapDlg::Tic()
// {
//         QueryPerformanceFrequency(&m_nFreq);
//         QueryPerformanceCounter(&m_nBeginTime);
// }

// double CVidCapDlg::Toc()
// {
//         LARGE_INTEGER nEndTime;
//         double nCalcTime;

//         QueryPerformanceCounter(&nEndTime);
//         nCalcTime = double((nEndTime.QuadPart - m_nBeginTime.QuadPart) * 1000) / double(m_nFreq.QuadPart);

//         return nCalcTime;
// }
BOOL CVidCapDlg::calHistForWholeImage(BYTE* LBP_image, double* hist)
{
	for (int i =0 ; i<256*NUM_OF_PATCH*NUM_OF_PATCH;i++)
		hist[i] = 0;

	double ix = FACE_VERIFY_WIDTH - LBP_R*2;
	double iy = FACE_VERIFY_HEIGHT - LBP_R*2;
	int sx = round(ix/double(NUM_OF_PATCH));
	int sy = round(iy/double(NUM_OF_PATCH));
	int cc = 0;

	for (int i = 0;i<NUM_OF_PATCH;i++)
	{
		int y_start = round(iy/double(NUM_OF_PATCH)*double(i));
		if (y_start + sy > iy)
			y_start = int(iy -sy + 1);
		for (int j = 0;j<NUM_OF_PATCH;j++)
		{
			int x_start = round(ix/double(NUM_OF_PATCH)*double(j));
			if (x_start + sx >  ix)
				x_start = int(ix -sx + 1);

			for (int ii = 0;ii<sx;ii++)
			{
				for (int jj=0;jj<sy;jj++)
				{
					int x_pos = x_start + ii;
					int y_pos = y_start + jj;
					hist[cc*256+ LBP_image[x_pos*int(iy)+y_pos]]++;
				}
			}

			for (int ii=0;ii<256;ii++)
				hist[cc*256+ii] /= sx*sy;
			cc++;
		}
	}
	return true;
}

BOOL CVidCapDlg::calLBPImg(BYTE* Image,BYTE* LBPMap)
{
	double offset_x[8] = {2, sqrt(2.0f), 0, -sqrt(2.0f), -2, -sqrt(2.0f), 0, sqrt(2.0f)};
	double offset_y[8] = {0, -sqrt(2.0f), -2, -sqrt(2.0f), 0, sqrt(2.0f), 2, sqrt(2.0f)};
	int hb = LBP_R;
	int dy = FACE_VERIFY_HEIGHT - LBP_R * 2;
	int dx = FACE_VERIFY_WIDTH - LBP_R * 2;

	for (int i = 0;i<dx*dy;i++)
		LBPMap[i] = 0;

	int LBP_bit;
	for (int i = 0;i<dx;i++)
	{
		for (int j = 0;j<dy;j++)
		{
			for (int k = 0;k<LBP_P;k++)
			{
				double xx = offset_x[k] + hb;
				double yy = offset_y[k] + hb;
				int rx = round(xx);
				int ry = round(yy);

				if ((abs(xx - rx) < 0.000001) && (abs(yy - ry) < 0.000001))
				{
					LBP_bit = Image[(rx+i)*FACE_VERIFY_HEIGHT+ry+j]>=Image[(i+hb)*FACE_VERIFY_HEIGHT+j+hb];
				}
				else
				{
					double fx = floor(xx);
					double fy = floor(yy);
					double cx = ceil(xx);
					double cy = ceil(yy);
					double tx = xx-fx;
					double ty = yy-fy;
					double w1 = (1 - tx) * (1 - ty);
					double w2 =      tx  * (1 - ty);
					double w3 = (1 - tx) *      ty ;
					double w4 =      tx  *      ty ;
					double N = 0;
					N += w1*double(Image[int((fx+i)*FACE_VERIFY_HEIGHT+fy+j)]);
					N += w2*double(Image[int((cx+i)*FACE_VERIFY_HEIGHT+fy+j)]);
					N += w3*double(Image[int((fx+i)*FACE_VERIFY_HEIGHT+cy+j)]);
					N += w4*double(Image[int((cx+i)*FACE_VERIFY_HEIGHT+cy+j)]);
					int tt = (abs(N-double(Image[(i+hb)*FACE_VERIFY_HEIGHT+j+hb]))<0.000001);
					LBP_bit = N >=Image[(i+hb)*FACE_VERIFY_HEIGHT+j+hb];
					LBP_bit = LBP_bit | tt;
				}
				LBPMap[i*dy+j]+=LBP_bit*power(2,k);
			}
		}
	}
	return true;
}

int CVidCapDlg::power(int a, int b)
{
	int R = 1;
	for (int i = 0;i<b;i++)
		R *= a;

	return R;
}

int CVidCapDlg::round(double floatNumber)
{
	int INTNumber = int(floatNumber);
	if (floatNumber < INTNumber + 0.5)
		return INTNumber;
	else return (INTNumber+1);
}
bool CVidCapDlg::FaceVerification(IplImage* pImage, RECT* eyeRectL, RECT* eyeRectR, int i)
{

	double cum_face_d = 0;
	unsigned char* CorpFaceImg = new unsigned char[FACE_VERIFY_HEIGHT * FACE_VERIFY_WIDTH];

	////Image Rotation, corp, and scaling
	if (!GeoNormal((unsigned char*)pImage->imageData,eyeRectL,eyeRectR,CorpFaceImg))
        {
            delete [] CorpFaceImg;
            //delete [] Image;
            return false;
        }
	/* cv::Mat tempMat1 = cv::Mat(130,150,CV_8UC1,(void*)CorpFaceImg);
	imshow("fd", tempMat1);
*/
	BYTE *LBPMap = new BYTE[(FACE_VERIFY_HEIGHT-LBP_R*2)*(FACE_VERIFY_WIDTH-LBP_R*2)];
	calLBPImg(CorpFaceImg,LBPMap);

	//int t1 = clock();
			
	double* hist = new double[256*NUM_OF_PATCH*NUM_OF_PATCH];
	calHistForWholeImage(LBPMap,hist);

	/* cv::Mat tempMat1 = cv::Mat(126,146,CV_8UC1,(void*)LBPMap);
	imshow("fd", tempMat1);*/
	int ppl_detect = 0;
	int histSize = 256*NUM_OF_PATCH*NUM_OF_PATCH;
	double weight[49] = {	2, 1, 1, 1, 1, 1, 2,
							2, 4, 4, 1, 4, 4, 2,
							1, 1, 1, 0, 1, 1, 1,
							0, 1, 1, 0, 1, 1, 0,
							0, 1, 1, 1, 1, 1, 0,
							0, 1, 1, 2, 1, 1, 0,
							0, 1, 1, 1, 1, 1, 0};

	ppl_detect = 0;
	double minDist = 10000;

	for (int ppl = 0; ppl < NUM_OF_REGISTER_USER; ppl++)
	{
		//matrix multiplication
		for (int i = 0;i<NUM_OF_USER_TEMPLATE;i++)
		{
			//WatchList[ppl].verify_face[histSize*i]

			double chi2dist = 0;
			for (int j = 0;j<NUM_OF_PATCH*NUM_OF_PATCH;j++)
			{
				double chi2dist_P = 0;
				//double hh[256];
				for (int k = 0;k<256;k++)
				{
					chi2dist_P += (hist[j*256+k]-m_WatchList[ppl].verify_face[i*histSize+j*256+k])*(hist[j*256+k]-m_WatchList[ppl].verify_face[i*histSize+j*256+k])/(hist[j*256+k]+m_WatchList[ppl].verify_face[i*histSize+j*256+k]+1e-100);
				}
				chi2dist+=chi2dist_P*weight[j];
			}
			if (chi2dist<minDist)
			{
				minDist = chi2dist;
				ppl_detect = ppl;
			}
		}
	}
	if(ppl_detect == 0)
	{
		ifstream myReadFile;
		myReadFile.open("i2p_face_recognition_data/newUserName.txt");
		string name;
		//myReadFile >> name;
		//myReadFile.close();
		getline(myReadFile,name);
		widestr = std::string(name.begin(), name.end());
		widecstr = widestr.c_str();
		m_TrackList[i].user_name = widecstr;
		myReadFile.close();
	}
	else
		m_TrackList[i].user_name = m_WatchList[ppl_detect].user_name;

	//cout << widecstr;

	return TRUE;
}
bool CVidCapDlg::GeoNormal(BYTE* Image,RECT* eyeRectL, RECT* eyeRectR,BYTE* CorpFaceImg)
{
	double lx = (double(eyeRectL->left)+double(eyeRectL->right))/2;
	double ly = (double(eyeRectL->top)+double(eyeRectL->bottom))/2;
	double rx = (double(eyeRectR->left)+double(eyeRectR->right))/2;
	double ry = (double(eyeRectR->top)+double(eyeRectR->bottom))/2;

	double EyeDistance = sqrt((lx - rx)*(lx - rx) + (ly - ry)*(ly - ry));
	double sinRatio = (ry - ly)/EyeDistance;
	double cosRatio = (rx - lx)/EyeDistance;

	//below is calculate the starting and ending coordinates for corping and rotation
	double Win_L_Edge_x = lx - (rx - lx) * CORP_EYE_L_EDGE / CORP_EYE_DISTANCE;
	double Win_L_Edge_y = ly - (ry - ly) * CORP_EYE_L_EDGE / CORP_EYE_DISTANCE;

	double Win_Start_x = Win_L_Edge_x + CORP_EYE_T_EDGE * EyeDistance * sinRatio/CORP_EYE_DISTANCE;
	double Win_Start_y = Win_L_Edge_y - CORP_EYE_T_EDGE * EyeDistance * cosRatio/CORP_EYE_DISTANCE;

	////add the location of scanning window
	//Win_Start.x += scanWin.x;
	//Win_Start.y += scanWin.y;

	if (Win_Start_y < 0) Win_Start_y = 0;

	//below is for scaling
	double ScaleFactor = CORP_EYE_DISTANCE/EyeDistance;
	//int newHeight = (int)((double)corpWinHeight * ScaleFactor);
	//int newWidth = (int)((double)corpWinWidth * ScaleFactor);
	int newHeight = FACE_VERIFY_HEIGHT;
	int newWidth = FACE_VERIFY_WIDTH;

	for (int i = 0; i < FACE_VERIFY_HEIGHT; i++)
	{
		for (int j = 0; j < FACE_VERIFY_WIDTH; j++)
		{
			double tempPos_y = Win_Start_y + ((double)j * sinRatio + (double)i * cosRatio)/ScaleFactor;
			double tempPos_x = Win_Start_x + ((double)j * cosRatio - (double)i * sinRatio)/ScaleFactor;

			if (tempPos_x<0 || tempPos_x>640 || tempPos_y<0 || tempPos_y>480)
				return false;

			CorpFaceImg[j*FACE_VERIFY_HEIGHT+i] = Image[round(tempPos_y)*ImgWidth+round(tempPos_x)];
		}
	}

	return true;

}

bool CVidCapDlg::HistEQ(unsigned char* Image, int ImgSize)
{
	unsigned char* tempImg = new unsigned char[ImgSize];

	float* hgram = new float[64];
	for (int i = 0;i < 64;i++)
		hgram[i] = float(ImgSize)/64;
	int n = 256;

	int* nn = new int[256];
	for (int i=0;i<256;i++)
		nn[i]=0;

	for (int i=0;i<ImgSize;i++)
		nn[Image[i]]++;

	int* cum = new int[256];
	cum[0] = nn[0];
	for (int i=1;i<256;i++)
		cum[i]=cum[i-1]+nn[i];

	int tt[256] = {0};
	for (int i = 0;i<256;i++)
		tt[i] = cum[i];


	float* cumd = new float[64];
    //	int cumd[64];

	cumd[0] = hgram[0];
	for (int i=1;i<64;i++)
		cumd[i] = cumd[i-1]+hgram[i];

	nn[0] = 0;
	nn[255] = 0;

	float** errMatrix = new float*[64];
	for (int i=0;i<64;i++)
		errMatrix[i] = new float[256];


	for (int i =0;i<64;i++)
        {
            for (int j=0;j<256;j++)
                {
                    errMatrix[i][j] = cumd[i]-float(cum[j]) + float(nn[j])/2;
                    if (errMatrix[i][j]<-0.00037766)
                        errMatrix[i][j] = float(ImgSize);
                }
        }

	float temp = 0;

	float* dum = new float[256];
	int* T = new int[256];
	for (int j = 0;j<256;j++)
        {
            dum[j] = errMatrix[0][j];
            T[j] = 0;
            for (int i=0;i<64;i++)
                {
                    if (errMatrix[i][j]<dum[j])
                        {
                            dum[j] = temp;
                            T[j] = i;
                        }
                }
        }
	for (int i = 0; i < ImgSize; i++)
        {
            tempImg[i] = round(T[Image[i]]*4.047619);//*255/63;
        }

	memcpy(Image,tempImg,ImgSize);

	delete [] tempImg;
	for (int i=0;i<64;i++)
		delete [] errMatrix[i];
	delete [] errMatrix;
	delete [] dum;
	delete [] T;
	delete [] cum;
	delete [] cumd;
	delete [] hgram;
	delete [] nn;
	return true;
}

bool CVidCapDlg::GrayLelNormal(unsigned char* Image, int ImgSize, double** NorImg)
{
	double sumImage = 0;
	double meanImage = 0;
	double sumSqImage = 0;
	double std = 0;
	for (int i = 0;i<ImgSize;i++)
		sumImage+=Image[i];
	meanImage = sumImage/ImgSize;
	for (int i = 0; i<ImgSize;i++)
		sumSqImage += (Image[i]-meanImage)*(Image[i]-meanImage);

	//take note that here we use n-1 as denominator instead of n
	std = sumSqImage/(ImgSize - 1);
	std = sqrt(std);

	for (int i = 0; i<ImgSize;i++)
        {
            (*NorImg)[i] = (Image[i] - meanImage)/std;
        }
	return TRUE;
}



static int Bpp(cv::Mat img) { return 8 * img.channels(); } 
