#include "StdAfx.h"
#include <windows.h>
#include <cstdlib>

#include <stdio.h>
#include <string.h>
#include <atlbase.h>
#include "sphelper.h"
// we can compile language only if we use windows cmd, it won't work with git command shell 
#include "grammar.h"
//Copyright (c) Microsoft Corporation. All rights reserved.

#include "VidCapDlgNao.h"
#include "highgui.h"


#include "KinectReader.h"
#include <iostream>
#include <queue>
#include <ctype.h>
#include <sys/timeb.h>
#include <math.h>
#include "Global.h"
#include "UserDetail.h"
bool flagToTriggerSnapshot = false;
bool takeSnapshot = false;
using namespace std;
using namespace cv;
static const int        cColorWidth  = 640;
static const int        cColorHeight = 480;
static const int        cStatusMessageMaxLen = MAX_PATH*2;
unsigned int KinectReader::m_nRes[4][2] = {{80, 60}, {320, 240}, {640, 480}, {1280, 960}};
HRESULT hr = E_FAIL;
CComPtr<ISpRecognizer> cpRecognizer;
CComPtr<ISpRecoContext> cpRecoCtxt;
CComPtr<ISpRecoGrammar> cpGrammar;
WCHAR * pchStop;
CComPtr<ISpRecoResult> cpResult;
// action template (mean must be subtracted!!!)
double KinectReader::ActionTemplate[6][12] = 
{
	{1,0,1,3,4,5,2,4,6,7,1,2},
	{1,0,1,3,4,5,2,4,6,7,1,2},
	{1,0,1,3,4,5,2,4,6,7,1,2},
	{1,0,1,3,4,5,2,4,6,7,1,2},
	{1,0,1,3,4,5,2,4,6,7,1,2},
	{1,0,1,3,4,5,2,4,6,7,1,2}
};

// root of the sum of power value of action templates
double KinectReader::ActionTemplatePowSumRoot[6] = {12.7279,3,3,3,3,3};	

KinectReader::KinectReader(void)
{
	//Init();
}

KinectReader::~KinectReader(void)
{
	Clear();
}

void KinectReader::Init(ProtectedClient<imi::WorldFusionServiceClient> * w_client/*,ProtectedClient<imi::SoundServiceClient> * s_client*/)
{
	m_bIsInit = false;	
	m_fFrameScale = 1.0;	
	_w_client = w_client;
	//_s_client = s_client;
	m_pNuiSensor = NULL;
	m_hNextDepthFrameEvent = NULL;
	m_hNextColorFrameEvent = NULL;
	m_hNextSkeletonEvent = NULL;
	m_pDepthStreamHandle = NULL;
	m_pColorStreamHandle = NULL;	
	m_svSkelTrackMode = SV_TRACKED_SKELETONS_STICKY1;
	m_nStickySkeletonIds[0] = m_nStickySkeletonIds[1] = 0;

	m_numAction = 6;
	m_lenFeaKinect = 12;	
}

inline HRESULT KinectReader::BlockForResult(ISpRecoContext * pRecoCtxt, ISpRecoResult ** ppResult)
{
    HRESULT hr = S_OK;
	CSpEvent event;

    printf("waiting to recognize...\n");
    while (SUCCEEDED(hr) &&
           SUCCEEDED(hr = event.GetFrom(pRecoCtxt)) &&
           hr == S_FALSE)
    {        
        hr = pRecoCtxt->WaitForNotifyEvent(INFINITE);
    }
    switch (event.eEventId)
        {
            case SPEI_RECOGNITION:
            {
                // ExecuteCommand(event.RecoResult(), hWnd);
                *ppResult = event.RecoResult();
                if (*ppResult)
                {
                    (*ppResult)->AddRef();
                }                
                printf("Done proper recogntion\n");
            }
        }
    // printf("done with recognition\n");
    return hr;
}

WCHAR * KinectReader::StopWord()
{
    WCHAR * pchStop;
    
    LANGID LangId = ::SpGetUserDefaultUILanguage();

    switch (LangId)
    {
        case MAKELANGID(LANG_JAPANESE, SUBLANG_DEFAULT):
            pchStop = L"\x7d42\x4e86\\\x30b7\x30e5\x30fc\x30ea\x30e7\x30fc/\x3057\x3085\x3046\x308a\x3087\x3046";;
            break;

        default:
            pchStop = L"bye";
            break;
    }

    return pchStop;
}

void KinectReader::Clear(void)
{
	if (m_bIsInit == true)
	{
		m_mtxColorImg.release();
		m_mtxDepthValues.release();
		m_mtxDepthData3D.release();
		m_hNextDepthFrameEvent = NULL;
		m_hNextColorFrameEvent = NULL;
		m_pDepthStreamHandle = NULL;
		m_pColorStreamHandle = NULL;
		m_bIsInit = false;
	}
}

bool KinectReader::OpenCam(void)
{
	HRESULT hr = Nui_Init();
	if ( FAILED(hr) )
	{
		printf("Fail to open the camera!\n");
		return false;
	}

	// set the initialization flag
	m_bIsInit = true;
	m_nFrmNum = 0;
	return true;
}
// this function display the color and depth image
void KinectReader::ViewVideo(void)
{
	while (1)
	{
		struct _timeb gsTimeStart, gsTimeEnd;
		_ftime(&gsTimeStart);

		GetOneFrame();
		ShowFrame();
	//	SaveFrame();
		GetActionCorr();

		_ftime(&gsTimeEnd);
		int nTimeCost =(gsTimeEnd.time * 1000 + gsTimeEnd.millitm) - (gsTimeStart.time * 1000 + gsTimeStart.millitm);
		printf("cost: %d \n", nTimeCost);

		if( cvWaitKey(10) >= 0 )
		{
			break;
		}	
	}
	cvDestroyWindow("DEPTH");
}

void KinectReader::CloseCam(void)
{
	if (m_bIsInit == true)
	{
		Nui_Clear();
		Clear();
	}
}

HRESULT KinectReader::Nui_Init( )
{
	HRESULT  hr;

	// init the sensor body
	if ( m_pNuiSensor == NULL )
	{
		HRESULT hr = NuiCreateSensorByIndex(0, &m_pNuiSensor);
		if ( FAILED(hr) )
			return hr;
	}
	m_hNextDepthFrameEvent = CreateEvent( NULL, TRUE, FALSE, NULL );
	m_hNextColorFrameEvent = CreateEvent( NULL, TRUE, FALSE, NULL );
	m_hNextSkeletonEvent = CreateEvent( NULL, TRUE, FALSE, NULL );

	// open the Kinect sensor with specified flags
	DWORD nuiFlags = NUI_INITIALIZE_FLAG_USES_DEPTH | NUI_INITIALIZE_FLAG_USES_SKELETON | NUI_INITIALIZE_FLAG_USES_COLOR;	
	hr = m_pNuiSensor->NuiInitialize( nuiFlags );   
	if ( E_NUI_SKELETAL_ENGINE_BUSY == hr )
	{
		nuiFlags = NUI_INITIALIZE_FLAG_USES_DEPTH | NUI_INITIALIZE_FLAG_USES_COLOR;
		hr = m_pNuiSensor->NuiInitialize( nuiFlags) ;
	}
	if ( FAILED( hr ) )
	{
		if ( E_NUI_DEVICE_IN_USE == hr )
			printf("This Kinect sensor is already in use\n");
		else
			printf("Failed to initialize Kinect\n");
		return hr;
	}

	// open the skeleton tracking function
	if (HasSkeletalEngine(m_pNuiSensor))
	{
// 		hr = m_pNuiSensor->NuiSkeletonTrackingEnable( m_hNextSkeletonEvent, 
// 			NUI_SKELETON_TRACKING_FLAG_ENABLE_IN_NEAR_RANGE );	
		hr = m_pNuiSensor->NuiSkeletonTrackingEnable( m_hNextSkeletonEvent, 
			NUI_SKELETON_TRACKING_FLAG_ENABLE_SEATED_SUPPORT );	//tracking model for seated situation
		if( FAILED( hr ) )
		{
			printf("IDS_ERROR_SKELETONTRACKING\n");
			return hr;
		}
	}
	
	// open the color sensor
	m_resColor = NUI_IMAGE_RESOLUTION_640x480;
	hr = m_pNuiSensor->NuiImageStreamOpen(
		NUI_IMAGE_TYPE_COLOR,
		m_resColor,
		0, 2,
		m_hNextColorFrameEvent,
		&m_pColorStreamHandle );
	m_sOrgColor = Size(m_nRes[m_resColor][0], m_nRes[m_resColor][1]);
	m_sColor = Size(m_nRes[m_resColor][0] * m_fFrameScale, m_nRes[m_resColor][1] * m_fFrameScale);

	if ( FAILED( hr ) )
	{
		printf("Could not open image stream color!\n");
		return hr;
	}
	
	// open the depth sensor
	m_resDepth = NUI_IMAGE_RESOLUTION_640x480;
	hr = m_pNuiSensor->NuiImageStreamOpen(
		NUI_IMAGE_TYPE_DEPTH,
		m_resDepth,
		0, 2,
		m_hNextDepthFrameEvent,
		&m_pDepthStreamHandle );
	m_sOrgDepth = Size(m_nRes[m_resDepth][0], m_nRes[m_resDepth][1]);
	m_sDepth = Size(m_nRes[m_resDepth][0] * m_fFrameScale, m_nRes[m_resDepth][1] * m_fFrameScale);

	if ( FAILED( hr ) )
	{
		printf("Could not open image stream depth!\n");
		return hr;
	}

	// allocate memory for images
	// color images should be resized to the same size of depth image
	m_mtxColorImg.create(m_sColor, CV_8UC3);
	m_mtxDepthData3D.create(m_sDepth, CV_32FC3);
	m_mtxDepthValues.create(m_sDepth, CV_32FC1);
	m_hEvents[0] = m_hNextDepthFrameEvent;
	m_hEvents[1] = m_hNextColorFrameEvent;
	m_hEvents[2] = m_hNextSkeletonEvent;
	return hr;

}

void KinectReader::Nui_Clear()
{
	if (m_pNuiSensor != NULL)
		m_pNuiSensor->NuiShutdown( );

	if ( m_hNextSkeletonEvent && ( m_hNextSkeletonEvent != INVALID_HANDLE_VALUE ) )
	{
		CloseHandle( m_hNextSkeletonEvent );
		m_hNextSkeletonEvent = NULL;
	}
	if ( m_hNextDepthFrameEvent && ( m_hNextDepthFrameEvent != INVALID_HANDLE_VALUE ) )
	{
		CloseHandle( m_hNextDepthFrameEvent );
		m_hNextDepthFrameEvent = NULL;
	}
	if ( m_hNextColorFrameEvent && ( m_hNextColorFrameEvent != INVALID_HANDLE_VALUE ) )
	{
		CloseHandle( m_hNextColorFrameEvent );
		m_hNextColorFrameEvent = NULL;
	}
	if (m_pNuiSensor != NULL)
	{
		m_pNuiSensor->Release();
		m_pNuiSensor = NULL;
	}

	m_pColorStreamHandle = NULL;
	m_pDepthStreamHandle = NULL;
}

bool KinectReader::GetOneFrame(void)
{
	int nEventIdx = WaitForMultipleObjects(3, m_hEvents, TRUE, 50);
	/*if (nEventIdx != WAIT_FAILED && nEventIdx != WAIT_TIMEOUT)
	{
		bool bDepth = Nui_NextDepthFrame();
		bool bColor = Nui_NextColorFrame();
		bool bSkeleton = Nui_NextSkeletonFrame();
		Nui_AlignFrames();
		m_nFrmNum++;
		return bColor && bDepth && bSkeleton;
	}
	else
	{
		printf("Fail to update frames!\n");
		return false;
	}*/
	// Main thread loop
    bool continueProcessing = true;
    while ( continueProcessing )
    {
        // Wait for any of the events to be signalled
        nEventIdx = WaitForMultipleObjects( 3, m_hEvents, TRUE, 50 );

        // Timed out, continue
        if ( nEventIdx == WAIT_TIMEOUT )
        {
            continue;
        }

        // stop event was signalled 
        if ( WAIT_OBJECT_0 == nEventIdx )
        {
            continueProcessing = false;
            //break;
        }

        // Wait for each object individually with a 0 timeout to make sure to
        // process all signalled objects if multiple objects were signalled
        // this loop iteration

        // In situations where perfect correspondance between color/depth/skeleton
        // is essential, a priority queue should be used to service the item
        // which has been updated the longest ago
		//Nui_AlignFrames();
		//m_nFrmNum++;
        if ( WAIT_OBJECT_0 == WaitForSingleObject( m_hNextDepthFrameEvent, 10 ) )
        {
           Nui_NextDepthFrame();
        }

        if ( WAIT_OBJECT_0 == WaitForSingleObject( m_hNextColorFrameEvent, 10 ) )
        {
            Nui_NextColorFrame();
        }

        if (  WAIT_OBJECT_0 == WaitForSingleObject( m_hNextSkeletonEvent, 10 ) )
        {
            Nui_NextSkeletonFrame( );
        }
    }
	//m_nFrmNum++;
	// FRAME EVENT ORDER: color then depth then skeleton, followed by 
	// an AllFramesReady event when all data frames are available.
/*	WaitForSingleObject(m_hNextDepthFrameEvent, 10);	
	bool bDepth = Nui_NextDepthFrame();
	WaitForSingleObject(m_hNextColorFrameEvent, 10); 
	bool bColor = Nui_NextColorFrame();
	WaitForSingleObject(m_hNextSkeletonEvent, 10); 
	bool bSkeleton = Nui_NextSkeletonFrame();
	m_nFrmNum++;
	return bColor && bDepth && bSkeleton;*/
	return 0;
}

bool KinectReader::Nui_NextColorFrame(void)
{
	HRESULT hr = m_pNuiSensor->NuiImageStreamGetNextFrame( m_pColorStreamHandle, 0, &m_imageColorFrame );

	if ( FAILED( hr ) )
	{
		printf("Fail to get next color frame!\n");
		return false;
	}


	//Take the snapshot from kinect camera and save the image

	INuiFrameTexture* pTexture = m_imageColorFrame.pFrameTexture;
	NUI_LOCKED_RECT LockedRect;
	pTexture->LockRect( 0, &LockedRect, NULL, 0 );
	if ( LockedRect.Pitch != 0 )
	{
		hr = SaveBitmapToFile(static_cast<BYTE *>(LockedRect.pBits), cColorWidth, cColorHeight, 32, "snapshot.bmp");

		if (SUCCEEDED(hr))
		{
			flagToTriggerSnapshot = true;
		}
		else
		{
			flagToTriggerSnapshot = false;
		}


		unsigned char* pRGBBuffer = (unsigned char*) LockedRect.pBits;

		// store the RGB data to original frame	
		int nChannels = 4;
		int nWStep = m_sOrgColor.width * 4;
		for (int i = 0; i < m_sColor.height; i++)
		{
			for (int j = 0; j < m_sColor.width; j++)
			{
				// the resized coordinates
				int jj = (int)(1.0 * j / m_sColor.width * m_sOrgColor.width);
				int ii = (int)(1.0 * i / m_sColor.height * m_sOrgColor.height);

				m_mtxColorImg.at<Vec3b>(i, j)[0] = *(pRGBBuffer + nWStep * ii + nChannels * jj);
				m_mtxColorImg.at<Vec3b>(i, j)[1] = *(pRGBBuffer + nWStep * ii + nChannels * jj + 1);
				m_mtxColorImg.at<Vec3b>(i, j)[2] = *(pRGBBuffer + nWStep * ii + nChannels * jj + 2);
			}
		}
	}
	else
	{
		printf("Buffer length of received texture is bogus!\n" );
		return false;
	}
	pTexture->UnlockRect(0);
	m_pNuiSensor->NuiImageStreamReleaseFrame( m_pColorStreamHandle, &m_imageColorFrame );
	return true;
}

bool KinectReader::Nui_NextDepthFrame(void)
{
	HRESULT hr = m_pNuiSensor->NuiImageStreamGetNextFrame(m_pDepthStreamHandle, 0, &m_imageDepthFrame);

	if (FAILED( hr ))
	{
		printf("%d\n", m_nFrmNum);
		printf("Fail to get next depth frame!\n");
		return false;
	}
	
	INuiFrameTexture * pTexture = m_imageDepthFrame.pFrameTexture;
	NUI_LOCKED_RECT LockedRect;
	pTexture->LockRect( 0, &LockedRect, NULL, 0 );
	if ( 0 != LockedRect.Pitch )
	{
		unsigned char* pDepthBuffer = (unsigned char*) LockedRect.pBits;
		Mat  cvDepth16u(m_sOrgDepth.height, m_sOrgDepth.width, CV_16UC1);

		// read the depth data from Kinect
		memcpy(cvDepth16u.data, pDepthBuffer, m_sOrgDepth.width * m_sOrgDepth.height * 2);

		// calculate the real world coordinate of the depth cloud
		//!! resize is important to convert from the original image to the current image
		int xmin, ymin, xmax, ymax;
		xmin = m_mtxColorImg.cols - 1;
		ymin = m_mtxColorImg.rows - 1;
		xmax = ymax = 0;
		for(int i = 0; i < m_sDepth.height; ++i )
		{
			for(int j = 0; j < m_sDepth.width; ++j )
			{
				// the resized coordinates
				int jj = (int)(1.0 * j / m_sDepth.width * m_sOrgDepth.width);
				int ii = (int)(1.0 * i / m_sDepth.height * m_sOrgDepth.height);
				unsigned short nDepthIndex = ((unsigned short*)cvDepth16u.data + cvDepth16u.cols * ii+ cvDepth16u.channels() * jj)[0];	
				unsigned short nRealDepth = NuiDepthPixelToDepth(nDepthIndex);
				double fDepth = 1.0 * nRealDepth / 1000; //depth data in "meter"

				// use the original depth value in minimeters
				Point pStdProj;
				pStdProj.x = 1.0 * jj / m_sOrgDepth.width * 640;	//resize the image to 480 * 640?
				pStdProj.y = 1.0 * ii / m_sOrgDepth.height * 480;

				// perform horizontal flip to align to the 3D hand model
				m_mtxDepthData3D.at<Vec3f>(i, m_sDepth.width - 1 - j) = FrameToWorld(pStdProj.x, pStdProj.y, fDepth, m_fFrameScale);	//real world coordinates (in "m")!!
				m_mtxDepthValues.at<float>(i, m_sDepth.width - 1 - j) = fDepth;

				// update the alignment rectangle
				int nMargin = 40;
				if (ii <= nMargin || ii >= m_sOrgDepth.height - nMargin - 1 || 
					jj <= nMargin || jj >= m_sOrgDepth.width - nMargin - 1)
				{
					LONG plColorX, plColorY;
					m_pNuiSensor->NuiImageGetColorPixelCoordinatesFromDepthPixelAtResolution(
						m_resColor, m_resDepth, 
						&(m_imageDepthFrame.ViewArea),
						jj, ii, nDepthIndex,
						&plColorX, &plColorY);
					if (plColorX >= 0 && plColorX < xmin)					xmin = plColorX;
					if (plColorX < m_mtxColorImg.cols && plColorX > xmax)	xmax = plColorX;
					if (plColorY >= 0 && plColorY < ymin)					ymin = plColorY;
					if (plColorY < m_mtxColorImg.rows && plColorY > ymax)	ymax = plColorY;
				}
			}
		}
		m_rtAlign = Rect(xmin, ymin, xmax - xmin + 1, ymax - ymin + 1);
	}
	else
	{
		printf("Buffer length of received texture is bogus!\n" );
		return false;
	}

	pTexture->UnlockRect(0);
	m_pNuiSensor->NuiImageStreamReleaseFrame( m_pDepthStreamHandle, &m_imageDepthFrame );
	return true;
}

// mapping from the depth camera's (i, j, v) into world coordinate
// the requirements are the input depth image should have a size of (640, 480)
Vec3f KinectReader::FrameToWorld(int x0, int y0, double fDepth, double fFrameScale)
{
	// the below parameters comform to the viewport of the Kinect, 57 horizontal
	int xx0 = x0 / fFrameScale;
	int yy0 = y0 / fFrameScale;
	int x = xx0;
	int y = 480 - yy0;
	double fx_d = 1.0 / 5.9421434211923247e+02;
	double fy_d = 1.0 / 5.9104053696870778e+02;
	double cx_d = 3.3930780975300314e+02;
	double cy_d = 2.4273913761751615e+02;

	Vec3f v;
	v[0] = (x - cx_d) * fDepth * fx_d;
	v[1]= (y - cy_d) * fDepth * fy_d;
	v[2] = fDepth;
	return v;
}

// this function must accord to "DepthToWorld" 
Point KinectReader::WorldToFrame(Vec3f v, double fFrameScale)
{
	int x, y;
	double fx_d = 1.0 / 5.9421434211923247e+02;
	double fy_d = 1.0 / 5.9104053696870778e+02;
	double cx_d = 3.3930780975300314e+02;
	double cy_d = 2.4273913761751615e+02;

	x = v[0] / (fx_d * v[2]) + cx_d;
	y = 480 - v[1] / (fy_d * v[2]) + cy_d;
	return Point(x * fFrameScale, y * fFrameScale);
}

void KinectReader::GetLeftArmJoints(BasicMaths::Vector3 &vLElbow, 
	BasicMaths::Vector3 &vLWrist, BasicMaths::Vector3 &vLHand)
{
	vLElbow = m_vLElbow;
	vLWrist = m_vLWrist;
	vLHand = m_vLHand;
}

void KinectReader::GetRightArmJoints(BasicMaths::Vector3 &vRElbow, 
	BasicMaths::Vector3 &vRWrist, BasicMaths::Vector3 &vRHand)
{
	vRElbow = m_vRElbow;
	vRWrist = m_vRWrist;
	vRHand = m_vRHand;
}

void KinectReader::Nui_DrawBone(cv::Mat &mtxCanvas, const NUI_SKELETON_DATA &skel, 
	NUI_SKELETON_POSITION_INDEX bone0, NUI_SKELETON_POSITION_INDEX bone1)
{
	NUI_SKELETON_POSITION_TRACKING_STATE bone0State = skel.eSkeletonPositionTrackingState[bone0];
	NUI_SKELETON_POSITION_TRACKING_STATE bone1State = skel.eSkeletonPositionTrackingState[bone1];

	// If we can't find either of these joints, exit
	if (bone0State == NUI_SKELETON_POSITION_NOT_TRACKED ||
		bone1State == NUI_SKELETON_POSITION_NOT_TRACKED)
		return;

	// Don't draw if both points are inferred
	if (bone0State == NUI_SKELETON_POSITION_INFERRED && 
		bone1State == NUI_SKELETON_POSITION_INFERRED)
		return;

	// We assume all drawn bones are inferred unless BOTH joints are tracked
	if (bone0State == NUI_SKELETON_POSITION_TRACKED && 
		bone1State == NUI_SKELETON_POSITION_TRACKED)
		line(mtxCanvas, m_pJoints2D[bone0], m_pJoints2D[bone1], Scalar(255, 0, 0), 3);
	else
		line(mtxCanvas, m_pJoints2D[bone0], m_pJoints2D[bone1], Scalar(0, 255, 0), 3);
}

void KinectReader::Nui_DrawSkeleton(cv::Mat &mtxCanvas, const NUI_SKELETON_DATA &skel)
{
	// first get the joints of the skeleton
	for (int i = 0; i < NUI_SKELETON_POSITION_COUNT; i++)
	{
		LONG x, y;
		USHORT depth;
		NuiTransformSkeletonToDepthImage(skel.SkeletonPositions[i], &x, &y, &depth);
		m_pJoints3D[i] = BasicMaths::Vector3(skel.SkeletonPositions[i].x, skel.SkeletonPositions[i].y, skel.SkeletonPositions[i].z);
		m_pJoints2D[i] = Point(1.0 * x / 320 * m_sColor.width, 1.0 * y / 240 * m_sColor.height);
		if ( skel.eSkeletonPositionTrackingState[i] == NUI_SKELETON_POSITION_INFERRED )
			circle(mtxCanvas, m_pJoints2D[i], 3, Scalar(0, 255, 0), 1);
		else if ( skel.eSkeletonPositionTrackingState[i] == NUI_SKELETON_POSITION_TRACKED )
			circle(mtxCanvas, m_pJoints2D[i], 3, Scalar(255, 0, 0), 1);
	}

	// Render Torso
	Nui_DrawBone(mtxCanvas, skel, NUI_SKELETON_POSITION_HEAD, NUI_SKELETON_POSITION_SHOULDER_CENTER );
	Nui_DrawBone(mtxCanvas, skel, NUI_SKELETON_POSITION_SHOULDER_CENTER, NUI_SKELETON_POSITION_SHOULDER_LEFT );
	Nui_DrawBone(mtxCanvas, skel, NUI_SKELETON_POSITION_SHOULDER_CENTER, NUI_SKELETON_POSITION_SHOULDER_RIGHT );
	Nui_DrawBone(mtxCanvas, skel, NUI_SKELETON_POSITION_SHOULDER_CENTER, NUI_SKELETON_POSITION_SPINE );
	Nui_DrawBone( mtxCanvas, skel, NUI_SKELETON_POSITION_SPINE, NUI_SKELETON_POSITION_HIP_CENTER );
	Nui_DrawBone( mtxCanvas, skel, NUI_SKELETON_POSITION_HIP_CENTER, NUI_SKELETON_POSITION_HIP_LEFT );
	Nui_DrawBone( mtxCanvas, skel, NUI_SKELETON_POSITION_HIP_CENTER, NUI_SKELETON_POSITION_HIP_RIGHT );
	// Head and Center of Shoulders
	/*if (skel.dwTrackingID == m_nStickySkeletonIds[0])
	{*/
		m_vHead = m_pJoints3D[NUI_SKELETON_POSITION_HEAD];
		m_vCShoulder = m_pJoints3D[NUI_SKELETON_POSITION_SHOULDER_CENTER];
		m_ptHead = m_pJoints2D[NUI_SKELETON_POSITION_HEAD];
		m_ptCShoulder = m_pJoints2D[NUI_SKELETON_POSITION_SHOULDER_CENTER];
	//}

	// Left Arm
	Nui_DrawBone( mtxCanvas, skel, NUI_SKELETON_POSITION_SHOULDER_LEFT, NUI_SKELETON_POSITION_ELBOW_LEFT );
	Nui_DrawBone( mtxCanvas, skel, NUI_SKELETON_POSITION_ELBOW_LEFT, NUI_SKELETON_POSITION_WRIST_LEFT );
	Nui_DrawBone( mtxCanvas, skel, NUI_SKELETON_POSITION_WRIST_LEFT, NUI_SKELETON_POSITION_HAND_LEFT );
	/*if (skel.dwTrackingID == m_nStickySkeletonIds[0])
	{*/
		m_vLHand = m_pJoints3D[NUI_SKELETON_POSITION_HAND_LEFT];
		m_vLWrist = m_pJoints3D[NUI_SKELETON_POSITION_WRIST_LEFT];
		m_vLElbow = m_pJoints3D[NUI_SKELETON_POSITION_ELBOW_LEFT];
		m_vLShoulder = m_pJoints3D[NUI_SKELETON_POSITION_SHOULDER_LEFT];
		m_ptLHand = m_pJoints2D[NUI_SKELETON_POSITION_HAND_LEFT];
		m_ptLWrist = m_pJoints2D[NUI_SKELETON_POSITION_WRIST_LEFT];
		m_ptLElbow = m_pJoints2D[NUI_SKELETON_POSITION_ELBOW_LEFT];
		m_ptLShoulder = m_pJoints2D[NUI_SKELETON_POSITION_SHOULDER_LEFT];
	//}

	// Right Arm
	Nui_DrawBone( mtxCanvas, skel, NUI_SKELETON_POSITION_SHOULDER_RIGHT, NUI_SKELETON_POSITION_ELBOW_RIGHT );
	Nui_DrawBone( mtxCanvas, skel, NUI_SKELETON_POSITION_ELBOW_RIGHT, NUI_SKELETON_POSITION_WRIST_RIGHT );
	Nui_DrawBone( mtxCanvas, skel, NUI_SKELETON_POSITION_WRIST_RIGHT, NUI_SKELETON_POSITION_HAND_RIGHT );
	/*if (skel.dwTrackingID == m_nStickySkeletonIds[0])
	{*/
		m_vRHand = m_pJoints3D[NUI_SKELETON_POSITION_HAND_RIGHT];
		m_vRWrist = m_pJoints3D[NUI_SKELETON_POSITION_WRIST_RIGHT];
		m_vRElbow = m_pJoints3D[NUI_SKELETON_POSITION_ELBOW_RIGHT];
		m_vRShoulder = m_pJoints3D[NUI_SKELETON_POSITION_SHOULDER_RIGHT];
		m_ptRHand = m_pJoints2D[NUI_SKELETON_POSITION_HAND_RIGHT];
		m_ptRWrist = m_pJoints2D[NUI_SKELETON_POSITION_WRIST_RIGHT];
		m_ptRElbow = m_pJoints2D[NUI_SKELETON_POSITION_ELBOW_RIGHT];
		m_ptRShoulder = m_pJoints2D[NUI_SKELETON_POSITION_SHOULDER_RIGHT];
	//}

	//// Left Leg
	//Nui_DrawBone( mtxCanvas, skel, NUI_SKELETON_POSITION_HIP_LEFT, NUI_SKELETON_POSITION_KNEE_LEFT );
	//Nui_DrawBone( mtxCanvas, skel, NUI_SKELETON_POSITION_KNEE_LEFT, NUI_SKELETON_POSITION_ANKLE_LEFT );
	//Nui_DrawBone( mtxCanvas, skel, NUI_SKELETON_POSITION_ANKLE_LEFT, NUI_SKELETON_POSITION_FOOT_LEFT );

	//// Right Leg
	//Nui_DrawBone( mtxCanvas, skel, NUI_SKELETON_POSITION_HIP_RIGHT, NUI_SKELETON_POSITION_KNEE_RIGHT );
	//Nui_DrawBone( mtxCanvas, skel, NUI_SKELETON_POSITION_KNEE_RIGHT, NUI_SKELETON_POSITION_ANKLE_RIGHT );
	//Nui_DrawBone( mtxCanvas, skel, NUI_SKELETON_POSITION_ANKLE_RIGHT, NUI_SKELETON_POSITION_FOOT_RIGHT );
}

void KinectReader::UpdateTrackedSkeletons( const NUI_SKELETON_FRAME & skel )
{
	DWORD nearestIDs[2] = { 0, 0 };
	USHORT nearestDepths[2] = { NUI_IMAGE_DEPTH_MAXIMUM, NUI_IMAGE_DEPTH_MAXIMUM };

	// purge old sticky skeleton IDs, if the user has left the frame, etc
	bool stickyID0Found = false;
	bool stickyID1Found = false;
	for ( int i = 0 ; i < NUI_SKELETON_COUNT; i++ )
	{
		NUI_SKELETON_TRACKING_STATE trackingState = skel.SkeletonData[i].eTrackingState;

		if ( trackingState == NUI_SKELETON_TRACKED || trackingState == NUI_SKELETON_POSITION_ONLY )
		{
			if ( skel.SkeletonData[i].dwTrackingID == m_nStickySkeletonIds[0] )
			{
				stickyID0Found = true;
			}
			else if ( skel.SkeletonData[i].dwTrackingID == m_nStickySkeletonIds[1] )
			{
				stickyID1Found = true;
			}
		}
	}

	if ( !stickyID0Found && stickyID1Found )
	{
		m_nStickySkeletonIds[0] = m_nStickySkeletonIds[1];
		m_nStickySkeletonIds[1] = 0;
	}
	else if ( !stickyID0Found )
	{
		m_nStickySkeletonIds[0] = 0;
	}
	else if ( !stickyID1Found )
	{
		m_nStickySkeletonIds[1] = 0;
	}

	// Calculate nearest and sticky skeletons
	for ( int i = 0 ; i < NUI_SKELETON_COUNT; i++ )
	{
		NUI_SKELETON_TRACKING_STATE trackingState = skel.SkeletonData[i].eTrackingState;

		if ( trackingState == NUI_SKELETON_TRACKED || trackingState == NUI_SKELETON_POSITION_ONLY )
		{
			// Save SkeletonIds for sticky mode if there's none already saved
			if ( 0 == m_nStickySkeletonIds[0] && m_nStickySkeletonIds[1] != skel.SkeletonData[i].dwTrackingID )
			{
				m_nStickySkeletonIds[0] = skel.SkeletonData[i].dwTrackingID;
			}
			else if ( 0 == m_nStickySkeletonIds[1] && m_nStickySkeletonIds[0] != skel.SkeletonData[i].dwTrackingID )
			{
				m_nStickySkeletonIds[1] = skel.SkeletonData[i].dwTrackingID;
			}

			LONG x, y;
			USHORT depth;

			// calculate the skeleton's position on the screen
			NuiTransformSkeletonToDepthImage( skel.SkeletonData[i].Position, &x, &y, &depth );

			if ( depth < nearestDepths[0] )
			{
				nearestDepths[1] = nearestDepths[0];
				nearestIDs[1] = nearestIDs[0];

				nearestDepths[0] = depth;
				nearestIDs[0] = skel.SkeletonData[i].dwTrackingID;
			}
			else if ( depth < nearestDepths[1] )
			{
				nearestDepths[1] = depth;
				nearestIDs[1] = skel.SkeletonData[i].dwTrackingID;
			}
		}
	}

	if ( SV_TRACKED_SKELETONS_NEAREST1 == m_svSkelTrackMode || 
		SV_TRACKED_SKELETONS_NEAREST2 == m_svSkelTrackMode )
	{
		// Only track the closest single skeleton in nearest 1 mode
		if ( SV_TRACKED_SKELETONS_NEAREST1 == m_svSkelTrackMode )
		{
			nearestIDs[1] = 0;
		}
		m_pNuiSensor->NuiSkeletonSetTrackedSkeletons(nearestIDs);
	}

	if ( SV_TRACKED_SKELETONS_STICKY1 == m_svSkelTrackMode || 
		SV_TRACKED_SKELETONS_STICKY2 == m_svSkelTrackMode )
	{
		DWORD stickyIDs[2] = { m_nStickySkeletonIds[0], m_nStickySkeletonIds[1] };

		// Only track a single skeleton in sticky 1 mode
		if ( SV_TRACKED_SKELETONS_STICKY1 == m_svSkelTrackMode )
		{
			stickyIDs[1] = 0;
		}
		m_pNuiSensor->NuiSkeletonSetTrackedSkeletons(stickyIDs);
	}
}

bool KinectReader::Nui_NextSkeletonFrame(void)
{
	bool bFound = false;
	if (SUCCEEDED(m_pNuiSensor->NuiSkeletonGetNextFrame( 0, &m_imageSkeletonFrame)) )
	{
		for ( int i = 0; i < NUI_SKELETON_COUNT; i++ )
		{
			NUI_SKELETON_TRACKING_STATE trackingState = m_imageSkeletonFrame.SkeletonData[i].eTrackingState;
			if (trackingState == NUI_SKELETON_TRACKED || trackingState == NUI_SKELETON_POSITION_ONLY)
				bFound = true;
		}
	}

	// no skeletons!
	if(!bFound )
		return false;

	// smooth out the skeleton data
	HRESULT hr = m_pNuiSensor->NuiTransformSmooth(&m_imageSkeletonFrame,NULL);
	if (FAILED(hr))
		return false;

	UpdateTrackedSkeletons(m_imageSkeletonFrame);
	return true;
}
HRESULT KinectReader::SaveBitmapToFile(BYTE* pBitmapBits, LONG lWidth, LONG lHeight, WORD wBitsPerPixel, std::string path)
{
    DWORD dwByteCount = lWidth * lHeight * (wBitsPerPixel / 8);

    BITMAPINFOHEADER bmpInfoHeader = {0};

    bmpInfoHeader.biSize        = sizeof(BITMAPINFOHEADER);  // Size of the header
    bmpInfoHeader.biBitCount    = wBitsPerPixel;             // Bit count
    bmpInfoHeader.biCompression = BI_RGB;                    // Standard RGB, no compression
    bmpInfoHeader.biWidth       = lWidth;                    // Width in pixels
    bmpInfoHeader.biHeight      = -lHeight;                  // Height in pixels, negative indicates it's stored right-side-up
    bmpInfoHeader.biPlanes      = 1;                         // Default
    bmpInfoHeader.biSizeImage   = dwByteCount;               // Image size in bytes

    BITMAPFILEHEADER bfh = {0};

    bfh.bfType    = 0x4D42;                                           // 'M''B', indicates bitmap
    bfh.bfOffBits = bmpInfoHeader.biSize + sizeof(BITMAPFILEHEADER);  // Offset to the start of pixel data
    bfh.bfSize    = bfh.bfOffBits + bmpInfoHeader.biSizeImage;        // Size of image + headers


	std::wstring stemp = std::wstring(path.begin(), path.end());
	LPCWSTR sw = stemp.c_str();
    // Create the file on disk to write to
    HANDLE hFile = CreateFileW(sw, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

    // Return if error opening file
    if (NULL == hFile) 
    {
        return E_ACCESSDENIED;
    }

    DWORD dwBytesWritten = 0;
    
    // Write the bitmap file header
    if ( !WriteFile(hFile, &bfh, sizeof(bfh), &dwBytesWritten, NULL) )
    {
        CloseHandle(hFile);
        return E_FAIL;
    }
    
    // Write the bitmap info header
    if ( !WriteFile(hFile, &bmpInfoHeader, sizeof(bmpInfoHeader), &dwBytesWritten, NULL) )
    {
        CloseHandle(hFile);
        return E_FAIL;
    }
    
    // Write the RGB Data
    if ( !WriteFile(hFile, pBitmapBits, bmpInfoHeader.biSizeImage, &dwBytesWritten, NULL) )
    {
        CloseHandle(hFile);
        return E_FAIL;
    }    

    // Close the file
    CloseHandle(hFile);
    return S_OK;
}

/// <summary>
/// Get the name of the file where screenshot will be stored.
/// </summary>
/// <param name="screenshotName">
/// [out] String buffer that will receive screenshot file name.
/// </param>
/// <param name="screenshotNameSize">
/// [in] Number of characters in screenshotName string buffer.
/// </param>
/// <returns>
/// S_OK on success, otherwise failure code.
/// </returns>
HRESULT GetScreenshotFileName(wchar_t *screenshotName, UINT screenshotNameSize)
{
    StringCchPrintfW(screenshotName, screenshotNameSize, L"KinectSnapshot.bmp");
    return hr;
}

void KinectReader::Nui_AlignFrames(void)
{
	cv::Mat mtxAlignColor(m_mtxColorImg.rows, m_mtxColorImg.cols, CV_8UC3);
	double fHScale = 1.0 * m_mtxColorImg.cols / m_rtAlign.width;
	double fVScale = 1.0 * m_mtxColorImg.rows / m_rtAlign.height;
	for (int i = 0; i < m_mtxColorImg.rows; i++)
	{
		for (int j = 0; j < m_mtxColorImg.cols; j++)
		{
			int jj = j / fHScale + m_rtAlign.x;
			int ii = i / fVScale + m_rtAlign.y;
			mtxAlignColor.at<cv::Vec3b>(i, j) = m_mtxColorImg.at<cv::Vec3b>(ii, jj);
		}
	}
	mtxAlignColor.copyTo(m_mtxColorImg);
}

void KinectReader::ShowFrame(void)
{
	//Start of Check sound service
	/*imi::AudioLocalization source;
	_s_client->getClient()->sound("Mic Array",imi::getTimeStamp(),source);
	double angle = source.azimuth;*/

	//End of Check sound service
	
	rectangle(m_mtxColorImg, Point(m_rtAlign.x, m_rtAlign.y),
		Point(m_rtAlign.x + m_rtAlign.width - 1, m_rtAlign.y + m_rtAlign.height - 1), Scalar(255, 0, 0), 2);
	static int snapshot =0;
	for ( int i = 0 ; i < NUI_SKELETON_COUNT; i++ )
	{
		NUI_SKELETON_TRACKING_STATE trackingState = m_imageSkeletonFrame.SkeletonData[i].eTrackingState;
		std::string name;
		if ( trackingState == NUI_SKELETON_TRACKED )
		{			
			 //update user status
            UserDetail* userDetail = m_listUsers.getUserFromId(i);
			if(userDetail == NULL)
			{
                m_listUsers.addUser(i);
                userDetail = m_listUsers.getUserFromId(i);
                userDetail->setVisibility(true);
                //m_client.getClient()->userNew(deviceName,imi::getTimeStamp(),i);
                                
                userDetail->setStatus(UserDetail::NEW);				

				////Take the snapshot from kinect camera and save the image
				//HRESULT hr;
				//NUI_IMAGE_FRAME imageFrame;

				//// Attempt to get the color frame
				//hr = m_pNuiSensor->NuiImageStreamGetNextFrame(m_pColorStreamHandle, 0, &imageFrame);
				//if (FAILED(hr))
				//{
				//	std::cout<< "error is getting color frame" << endl;
				//}
				//INuiFrameTexture * pTexture = imageFrame.pFrameTexture;
				//if(pTexture && SUCCEEDED(hr))
				//{
				//	NUI_LOCKED_RECT LockedRect;

				//	// Lock the frame data so the Kinect knows not to modify it while we're reading it
				//	pTexture->LockRect(0, &LockedRect, NULL, 0);
				//	// Load the image using the application and save the name with respective id
				//	// Make sure we've received valid data
				//	if (LockedRect.Pitch != 0)
				//	{
				//		// Write out the bitmap to disk
				//		hr = SaveBitmapToFile(static_cast<BYTE *>(LockedRect.pBits), cColorWidth, cColorHeight, 32, "snapshot.bmp");

				//		if (SUCCEEDED(hr))
				//		{
				//			// call the face recogntion function here with the file location

				//			CVidCapDlg obj(std::string("cam!"));
				//			cv::Mat frame = cv::imread("snapshot.bmp");
				//			std::string name = obj.init(frame);
				//			if(!name.empty())
				//			{
				//				userDetail->setName(name);
				//				_w_client->getClient()->userEntered("kinect1",imi::getTimeStamp(),i,userDetail->getName(),m_listUsers._details);
				//			}
				//		
				//			/*_w_client->getClient()->userEntered("kinect1",imi::getTimeStamp(),i,userDetail->getName(),m_listUsers._details);*/
				//		
				//		}
				//		else
				//		{
				//			// do nothing for now
				//		}
				//	}
				//}

				if(flagToTriggerSnapshot)
				{
					CVidCapDlg obj(std::string("cam!"));
					cv::Mat frame = cv::imread("snapshot.bmp");
					std::string name = obj.init(frame);
					if(!name.empty())
					{
						userDetail->setName(name);
						_w_client->getClient()->userEntered("kinect1",imi::getTimeStamp(),i,userDetail->getName(),m_listUsers._details);
					}
				}
			}
			if(userDetail!= NULL && (userDetail->getName() == "unknown") && m_imageSkeletonFrame.SkeletonData[i].SkeletonPositions[3].z < 1.3)
			{
				//Take the snapshot from kinect camera and save the image
				//HRESULT hr;
				//NUI_IMAGE_FRAME imageFrame;

				//// Attempt to get the color frame
				//hr = m_pNuiSensor->NuiImageStreamGetNextFrame(m_pColorStreamHandle, 0, &imageFrame);
				//if (FAILED(hr))
				//{
				//	std::cout<< "error is getting color frame" << endl;
				//}
				//INuiFrameTexture * pTexture = imageFrame.pFrameTexture;
				//NUI_LOCKED_RECT LockedRect;
				//if(pTexture && SUCCEEDED(hr))
				//{
				//	// Lock the frame data so the Kinect knows not to modify it while we're reading it
				//	pTexture->LockRect(0, &LockedRect, NULL, 0);
				//	// Load the image using the application and save the name with respective id
				//	// Make sure we've received valid data
				//	if (LockedRect.Pitch != 0)
				//	{
				//		// Write out the bitmap to disk
				//		hr = SaveBitmapToFile(static_cast<BYTE *>(LockedRect.pBits), cColorWidth, cColorHeight, 32, "snapshot.bmp");

				//		if (SUCCEEDED(hr))
				//		{
				//			// call the face recogntion function here with the file location

				//			CVidCapDlg obj(std::string("cam!"));
				//			cv::Mat frame = cv::imread("snapshot.bmp");
				//			std::string name = obj.init(frame);
				//			if(!name.empty())
				//			{
				//				userDetail->setName(name);
				//			}
				//			else
				//			{
				//				std::string name = "UnRegistered";
				//				userDetail->setName(name);
				//			}
				//		}
				//		else
				//		{
				//			// do nothing for now
				//		}
				//	}
				//}
				if(flagToTriggerSnapshot)
				{
					CVidCapDlg obj(std::string("cam!"));
					cv::Mat frame = cv::imread("snapshot.bmp");
					std::string name = obj.init(frame);
					if(!name.empty())
					{
						userDetail->setName(name);
					}
					else
					{
						std::string name = "UnRegistered";
						userDetail->setName(name);
					}
				}
			}
			if(userDetail!= NULL && (userDetail->getName() == "UnRegistered") && m_imageSkeletonFrame.SkeletonData[i].SkeletonPositions[3].z < 1.1 && snapshot <5)
			{
				////Take the snapshot from kinect camera and save the image
				//HRESULT hr;
				//NUI_IMAGE_FRAME imageFrame;

				//// Attempt to get the color frame
				//hr = m_pNuiSensor->NuiImageStreamGetNextFrame(m_pColorStreamHandle, 0, &imageFrame);
				//if (FAILED(hr))
				//{
				//	std::cout<< "error is getting color frame" << endl;
				//}
				//INuiFrameTexture * pTexture = imageFrame.pFrameTexture;
				//NUI_LOCKED_RECT LockedRect;
				//if(pTexture && SUCCEEDED(hr))
				//{
				//	// Lock the frame data so the Kinect knows not to modify it while we're reading it
				//	pTexture->LockRect(0, &LockedRect, NULL, 0);
				//	// Load the image using the application and save the name with respective id
				//	// Make sure we've received valid data
				//	if (LockedRect.Pitch != 0)
				//	{
				//		// Write out the bitmap to disk
				//		hr = SaveBitmapToFile(static_cast<BYTE *>(LockedRect.pBits), cColorWidth, cColorHeight, 32, "snapshot.bmp");

				//		if (SUCCEEDED(hr))
				//		{
				//			// call the face recogntion function here with the file location

				//			CVidCapDlg obj(std::string("cam!"));
				//			cv::Mat frame = cv::imread("snapshot.bmp");
				//			std::string name = obj.init(frame);
				//			if(!name.empty())
				//			{
				//				userDetail->setName(name);
				//			}
				//			else
				//			{
				//				std::string name = "UnRegistered";
				//				userDetail->setName(name);
				//				snapshot++;
				//			}
				//		}
				//		else
				//		{
				//			// do nothing for now
				//		}
				//	}
				//}
				if(flagToTriggerSnapshot)
				{
					CVidCapDlg obj(std::string("cam!"));
					cv::Mat frame = cv::imread("snapshot.bmp");
					std::string name = obj.init(frame);
					if(!name.empty())
					{
						userDetail->setName(name);
					}
					else
					{
						std::string name = "UnRegistered";
						userDetail->setName(name);
						snapshot++;
					}
				}
			}

			Nui_DrawSkeleton(m_mtxColorImg, m_imageSkeletonFrame.SkeletonData[i]);

			 //update user position
            for(int j=0; j< NUI_SKELETON_POSITION_COUNT; j++)
            {
                Vector4 pos = m_imageSkeletonFrame.SkeletonData[i].SkeletonPositions[j];
                imi::Vec3 imipos;
                imipos.x = pos.x; imipos.y = pos.y; imipos.z = pos.z;
				if(userDetail)
				{
					switch(j)
					{
						case NUI_SKELETON_POSITION_ANKLE_LEFT:
						{
							userDetail->m_positions.anklel = imipos;
							break;
						}
						case NUI_SKELETON_POSITION_ANKLE_RIGHT:
						{
							userDetail->m_positions.ankler = imipos;
							break;
						}
						case NUI_SKELETON_POSITION_ELBOW_LEFT:
						{
							userDetail->m_positions.elbowl = imipos;
							break;
						}
						case NUI_SKELETON_POSITION_ELBOW_RIGHT:
						{
							userDetail->m_positions.elbowr = imipos;
							break;
						}
						case NUI_SKELETON_POSITION_FOOT_LEFT:
						{
							userDetail->m_positions.footl = imipos;
							break;
						}
						case NUI_SKELETON_POSITION_FOOT_RIGHT:
						{
							userDetail->m_positions.footr = imipos;
							break;
						}
						case NUI_SKELETON_POSITION_HAND_LEFT:
						{
							userDetail->m_positions.handl = imipos;
							break;
						}
						case NUI_SKELETON_POSITION_HAND_RIGHT:
						{
							userDetail->m_positions.handr = imipos;
							break;
						}
						case NUI_SKELETON_POSITION_HEAD:
						{
							userDetail->m_positions.head = imipos;
							break;
						}
						case NUI_SKELETON_POSITION_HIP_CENTER:
						{
							userDetail->m_positions.hipc = imipos;
							break;
						}
						case NUI_SKELETON_POSITION_HIP_LEFT:
						{
							userDetail->m_positions.hipl = imipos;
							break;
						}
						case NUI_SKELETON_POSITION_HIP_RIGHT:
						{
							userDetail->m_positions.hipr = imipos;
							break;
						}
						case NUI_SKELETON_POSITION_KNEE_LEFT:
						{
							userDetail->m_positions.kneel = imipos;
							break;
						}
						case NUI_SKELETON_POSITION_KNEE_RIGHT:
						{
							userDetail->m_positions.kneer = imipos;
							break;
						}
						case NUI_SKELETON_POSITION_SHOULDER_CENTER:
						{
							userDetail->m_positions.shoulderc = imipos;
							break;
						}
						case NUI_SKELETON_POSITION_SHOULDER_LEFT:
						{
							userDetail->m_positions.shoulderl = imipos;
							break;
						}
						case NUI_SKELETON_POSITION_SHOULDER_RIGHT:
						{
							userDetail->m_positions.shoulderr = imipos;
							break;
						}
						case NUI_SKELETON_POSITION_SPINE:
						{
							userDetail->m_positions.spine = imipos;
							break;
						}
						case NUI_SKELETON_POSITION_WRIST_LEFT:
						{
							userDetail->m_positions.wristl = imipos;
							break;
						}
						case NUI_SKELETON_POSITION_WRIST_RIGHT:
						{
							userDetail->m_positions.wristr = imipos;
							break;
						}
					}
				}
            }

            //_w_client->getClient()->userSkeletonChanged("kinect1",imi::getTimeStamp(),userDetail->getId(),userDetail->m_positions);
			//_m_client->getClient()->userSkeletonChanged("kinect1",imi::getTimeStamp(),i,pos);
					
			// draw the key joints
			int circle_radius = 3;
			circle(m_mtxColorImg, m_ptHead, circle_radius, Scalar(0, 255, 255), 3);
			circle(m_mtxColorImg, m_ptCShoulder, circle_radius, Scalar(0, 255, 255), 3);
			circle(m_mtxColorImg, m_ptLHand, circle_radius, Scalar(0, 0, 255), 3);
			circle(m_mtxColorImg, m_ptLWrist, circle_radius, Scalar(0, 0, 255), 3);
			circle(m_mtxColorImg, m_ptLElbow, circle_radius, Scalar(0, 0, 255), 3);
			circle(m_mtxColorImg, m_ptLShoulder, circle_radius, Scalar(0, 0, 255), 3);
			circle(m_mtxColorImg, m_ptRHand, circle_radius, Scalar(0, 255, 0), 3);
			circle(m_mtxColorImg, m_ptRWrist, circle_radius, Scalar(0, 255, 0), 3);
			circle(m_mtxColorImg, m_ptRElbow, circle_radius, Scalar(0, 255, 0), 3);
			circle(m_mtxColorImg, m_ptRShoulder, circle_radius, Scalar(0, 255, 0), 3);
			
			// drawing a box on the face
			//rectangle(m_mtxColorImg,cvPoint(m_ptHead.x - 20,m_ptHead.y+10),cvPoint(m_ptHead.x + 20,m_ptHead.y-30),cvScalar(0, 255, 0, 0), 1, 8, 0);
			std::ostringstream s;
			if(userDetail)
			{
				s << "User " << userDetail->getId();
				std::string query(s.str());
				//writing user id
				//putText(m_mtxColorImg,s.str(), cvPoint(m_ptHead.x +20,m_ptHead.y+20), FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(0,255,0), 1, CV_AA);
				std::string newName = userDetail->getName();
				if(!newName.empty())
					putText(m_mtxColorImg,newName, cvPoint(m_ptHead.x +20,m_ptHead.y+5), FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(0,0,255), 1, CV_AA);
				/*if(newName == "Rohit")
				{*/
					putText(m_mtxColorImg,"Speaking", cvPoint(m_ptHead.x +20,m_ptHead.y-15), FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(0,0,255), 1, CV_AA);
					putText(m_mtxColorImg,"Waving", cvPoint(m_ptHead.x +20,m_ptHead.y-35), FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(0,0,255), 1, CV_AA);
				/*}
				else
				{*/
					putText(m_mtxColorImg,"Not Speaking", cvPoint(m_ptHead.x +20,m_ptHead.y-15), FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(0,0,255), 1, CV_AA);
					putText(m_mtxColorImg,"No Gesture", cvPoint(m_ptHead.x +20,m_ptHead.y-35), FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(0,0,255), 1, CV_AA);
				//}
			}
			imi::Skeleton pos;			
			
			pos.head.x = m_vHead.x;
			pos.head.y = m_vHead.y;
			pos.head.z = m_vHead.z;

			//_m_client->getClient()->userSkeletonChanged("kinect1",imi::getTimeStamp(),i,pos);
			
		}
		else if ( trackingState == NUI_SKELETON_POSITION_ONLY )
		{
			// we've only received the center point of the skeleton, draw that
			LONG x, y;
			USHORT depth;
			NuiTransformSkeletonToDepthImage(m_imageSkeletonFrame.SkeletonData[i].Position, &x, &y, &depth);
			//circle(m_mtxColorImg, Point(x, y), 5, Scalar(255, 0, 0), 2);
		}
        else
        {
            //update user status
            if(m_listUsers.getUserFromId(i) != NULL)
            {
                m_listUsers.eraseUser(i);
				//delete snapshot image
				remove("snapshot.bmp");
                //m_client.getClient()->userLost(deviceName,imi::getTimeStamp(),i);
				_w_client->getClient()->userLeft("kinect1",imi::getTimeStamp(),i,"unknown");
            }
        }
	}

	CvPoint ppt = cvPoint(1,1);

	imshow("color", m_mtxColorImg);
//	imshow("depth", m_mtxDepthValues);
	ShowActionType(1);	
}

void KinectReader::SaveFrame(void)
{
	// save the depth image to binary files
	char text[255];
	sprintf(text, "D:\\Pose_Database\\depth_%d.dat", m_nFrmNum);
	FILE *pFile = fopen(text, "wb");

	//relative coordinates to the joint of "center between shoulders"
	fprintf(pFile, "%f\t%f\t%f\t", (m_vHead.x-m_vCShoulder.x), (m_vHead.y-m_vCShoulder.y), (m_vHead.z-m_vCShoulder.z));	//head
	fprintf(pFile, "%f\t%f\t%f\t", (m_vRShoulder.x-m_vCShoulder.x), (m_vRShoulder.y-m_vCShoulder.y), (m_vRShoulder.z-m_vCShoulder.z));	//right shoulder
	fprintf(pFile, "%f\t%f\t%f\t", (m_vRElbow.x-m_vCShoulder.x), (m_vRElbow.y-m_vCShoulder.y), (m_vRElbow.z-m_vCShoulder.z));	//right elbow
	fprintf(pFile, "%f\t%f\t%f\t", (m_vRHand.x-m_vCShoulder.x), (m_vRHand.y-m_vCShoulder.y), (m_vRHand.z-m_vCShoulder.z));	//right hand

	fclose(pFile);

// 	for (int i = 0; i < m_sColor.height; i++)
// 	{
// 		for (int j = 0; j < m_sColor.width; j++)
// 		{
// 			double fDepthValue = m_mtxDepthData3D.at<Vec3f>(i, j).val[2];
// //			fwrite((void*)(&fDepthValue), sizeof(double), 1, pFile);
// //			fprintf(pFile, "%f\n", fDepthValue);  
// 		}
// 	}
// 	fclose(pFile);

// 	double kk[2] = {1,10};
// 	fwrite((void*)(kk), sizeof(double), 2, pFile);
	
// 	pFile = fopen(text, "rb");
// 	double tmp[2] = {0,0};
// 	fread((void*)(tmp), sizeof(double), 2, pFile);
// 	fclose(pFile);

// 	sprintf(text, "D:\\Pose_Database\\depth_%d.jpg", m_nFrmNum);
// 	double minDepth, maxDepth;
// 	minMaxLoc(m_mtxDepthValues, &minDepth, &maxDepth); //find minimum and maximum depth value
// 	Mat Depthdraw, Depthdraw_tmp;
// 	m_mtxDepthValues.convertTo(Depthdraw, CV_8U, 255.0/(maxDepth - minDepth), -minDepth * 255.0/(maxDepth - minDepth));
// 	m_mtxDepthValues.convertTo(Depthdraw_tmp, CV_8U, 255.0/(maxDepth - minDepth), -minDepth * 255.0/(maxDepth - minDepth));
// 	for(int i = 0; i < Depthdraw.rows; i++)
// 		for(int j = 0; j < Depthdraw.cols; j++)
// 		{
// 			{
// 				Depthdraw.at<UCHAR>(i,j) = Depthdraw_tmp.at<UCHAR>(i,Depthdraw.cols-j);
// 			}
// 		}
// 	imwrite(text, Depthdraw);

	sprintf(text, "D:\\Pose_Database\\color_%d.jpg", m_nFrmNum);
	imwrite(text, m_mtxColorImg);
}

double KinectReader::FeaMean(double *feaVector, int feaLength)
{
	int i = 0;

	double feaMean = 0;

	for (i = 0; i < feaLength; i++)
	{
		feaMean = feaMean + feaVector[i];
	}

	return feaMean / feaLength;
}

void KinectReader::GetActionFeature(void)
{
	m_feaKinect[0] = m_vHead.x-m_vCShoulder.x;		m_feaKinect[1] = m_vHead.y-m_vCShoulder.y;		m_feaKinect[2] = m_vHead.z-m_vCShoulder.z;		//head
	m_feaKinect[3] = m_vRShoulder.x-m_vCShoulder.x;	m_feaKinect[4] = m_vRShoulder.y-m_vCShoulder.y;	m_feaKinect[5] = m_vRShoulder.z-m_vCShoulder.z;	//right shoulder
	m_feaKinect[6] = m_vRElbow.x-m_vCShoulder.x;	m_feaKinect[7] = m_vRElbow.y-m_vCShoulder.y;	m_feaKinect[8] = m_vRElbow.z-m_vCShoulder.z;	//right elbow
	m_feaKinect[9] = m_vRHand.x-m_vCShoulder.x;		m_feaKinect[10] = m_vRHand.y-m_vCShoulder.y;	m_feaKinect[11] = m_vRHand.z-m_vCShoulder.z;	//right hand

// 	m_featureAction[0] = 1;		m_featureAction[1] = 0;		m_featureAction[2] = 1;		//head
// 	m_featureAction[3] = 3;	m_featureAction[4] = 4;	m_featureAction[5] = 5;	//right shoulder
// 	m_featureAction[6] = 2;	m_featureAction[7] = 4;	m_featureAction[8] = 6;	//right elbow
// 	m_featureAction[9] = 7;		m_featureAction[10] = 1;	m_featureAction[11] = 2;	//right hand
}

void KinectReader::GetActionCorr(void)
{
	GetActionFeature();	// calculate current kinect action feature vector

	int i = 0, j= 0;

	double sumTmp = 0, sumTmp1 = 0;

	double feaMean = FeaMean(m_feaKinect, m_lenFeaKinect);	// mean of current kinect action feature vector

	for (i=0; i < m_numAction; i++)
	{
		for (j=0; j < m_lenFeaKinect; j++)
		{
			double diffTmp = m_feaKinect[j] - feaMean;
			sumTmp = sumTmp + diffTmp * ActionTemplate[i][j];
			sumTmp1 = sumTmp1 + pow(diffTmp, 2.0);
		}

		if (sumTmp1 == 0)
		{
			m_corrAction[i] = 0;
		} 
		else
		{
			m_corrAction[i] = sumTmp / (sqrt(sumTmp1) * ActionTemplatePowSumRoot[i]);
		}
	}
		
}

void KinectReader::ShowActionType(int actionIdx)
{
	// background image
	cv::Mat actionIdxImg(300, 500, CV_8UC3, cv::Scalar(255,255,255));
	IplImage actionIdxImg1 = IplImage(actionIdxImg);

	// text font
	CvFont font; 
	cvInitFont(&font, CV_FONT_HERSHEY_DUPLEX,2, 2, 0, 1, CV_AA);
	
	// text size
	char text[30];
	CvSize textSize;
	int baseline;

	if (actionIdx == 1)
	{
		sprintf(text, "Have Question!");
	}

		
	cvGetTextSize(text, &font, &textSize, &baseline);
	cvPutText(&actionIdxImg1, text , cvPoint((int)(actionIdxImg.cols-textSize.width)/2, (int)(actionIdxImg.rows-textSize.height)/2), &font, CV_RGB(255,0,0));

	imshow("action", actionIdxImg);

}

// void KinectReader::GetActionCorr(void)
// {
// 	GetActionFeature();	// calculate current kinect action feature vector
// 
// 	int i = 0, j= 0;
// 
// 	double sumTmp = 0, sumTmp1 = 0, sumTmp2 = 0;
// 
// 	double feaMean = FeaMean(m_featureAction, m_lenFeatureKinect);	// mean of current kinect action feature vector
// 	double feaMean1 = 0;
// 
// 	for (i=0; i < m_numAction; i++)
// 	{
// 		feaMean1 = FeaMean(ActionTemplate[i], m_lenFeatureKinect);
// 
// 		for (j=0; j < m_lenFeatureKinect; j++)
// 		{
// 			sumTmp = sumTmp + (m_featureAction[j] - feaMean) * (ActionTemplate[i][j] - feaMean1);
// 			sumTmp1 = sumTmp1 + pow(m_featureAction[j] - feaMean, 2.0);
// 			sumTmp2 = sumTmp2 + pow(ActionTemplate[i][j] - feaMean, 2.0);
// 		}
// 
// 		if (sumTmp1 == 0)
// 		{
// 			m_corrAction[i] = 0;
// 		} 
// 		else
// 		{
// 			m_corrAction[i] = sumTmp / (sqrt(sumTmp1) * sqrt(sumTmp2));
// 		}
// 	}
// 
// }