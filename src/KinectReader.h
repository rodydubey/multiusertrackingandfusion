#pragma once
// I2p headers
#include "AgentControl.h"
#include "I2P_types.h"
#include "ThriftTools.hpp"
#include <sapi.h>
//General headers
#include "SpeechClient.h"
#include "FaceClient.h"
#include "FaceService.h"
#include "WorldFusionService.h"
#include "ProtectedClient.h"
#include "UserCount.h"
#include <highgui.h>
#include <stdio.h>
#include <string>
#include <cxcore.h>
#include <cv.h>

#include "NuiApi.h"
#include "Vector3.h"

enum SV_TRACKED_SKELETON_MODE
{
	SV_TRACKED_SKELETONS_DEFAULT = 0,
	SV_TRACKED_SKELETONS_NEAREST1,
	SV_TRACKED_SKELETONS_NEAREST2,
	SV_TRACKED_SKELETONS_STICKY1,
	SV_TRACKED_SKELETONS_STICKY2
};

class KinectReader
{
public:
	KinectReader(void);
	~KinectReader(void);

public:	
	void		Init(ProtectedClient<imi::WorldFusionServiceClient> * w_client/*,ProtectedClient<imi::SoundServiceClient> * s_client*/);
	void		Clear(void);
	bool		IsOpen(void) {return m_bIsInit;}
	bool		OpenCam(void);
	bool		GetOneFrame(void);
	void		ShowFrame(void);
	void		SaveFrame(void);
	void		CloseCam(void);
	void		ViewVideo(void);	
	int			GetFrameNum(void) {return m_nFrmNum;}
	void		GetActionFeature(void);		// calculate feature vector for action recognition
	
	void		detectVoice(void);

public:	
	cv::Mat&		GetDepthData3D(void) { return m_mtxDepthData3D;}
	cv::Mat&		GetColorImg(void) {return m_mtxColorImg;}
	cv::Mat&		GetDepthValues(void) { return m_mtxDepthValues;}
	void SetFrameScale(double fFrmScale)	{m_fFrameScale = fFrmScale;}
	double GetFrameScale(void)			{return m_fFrameScale;}
	void GetLeftArmJoints(BasicMaths::Vector3 &vLElbow, BasicMaths::Vector3 &vLWrist, BasicMaths::Vector3 &vLHand);
	void GetRightArmJoints(BasicMaths::Vector3 &vRElbow, BasicMaths::Vector3 &vRWrist, BasicMaths::Vector3 &vRHand);


public:
	static cv::Point	WorldToFrame(cv::Vec3f v, double fFrameScale);
	static cv::Vec3f	FrameToWorld(int x0, int y0, double fDepth, double fFrameScale);

private:
	HRESULT	Nui_Init(void);
	void			Nui_Clear(void);
	bool			Nui_NextColorFrame(void);
	bool			Nui_NextDepthFrame(void);
	bool			Nui_NextSkeletonFrame(void);
	void			Nui_AlignFrames(void);
	/// <summary>
    /// Save passed in image data to disk as a bitmap
    /// </summary>
    /// <param name="pBitmapBits">image data to save</param>
    /// <param name="lWidth">width (in pixels) of input image data</param>
    /// <param name="lHeight">height (in pixels) of input image data</param>
    /// <param name="wBitsPerPixel">bits per pixel of image data</param>
    /// <param name="lpszFilePath">full file path to output bitmap to</param>
    /// <returns>S_OK on success, otherwise failure code</returns>
    HRESULT SaveBitmapToFile(BYTE* pBitmapBits, LONG lWidth, LONG lHeight, WORD wBitsPerPixel, std::string path);

private:
	void Nui_DrawSkeleton(cv::Mat &mtxCanvas, const NUI_SKELETON_DATA & skel);
	void Nui_DrawBone(cv::Mat &mtxCanvas, const NUI_SKELETON_DATA &skel, NUI_SKELETON_POSITION_INDEX bone0,
		NUI_SKELETON_POSITION_INDEX bone1);
	void UpdateTrackedSkeletons(const NUI_SKELETON_FRAME & skel);	

private:		// current kinect
	INuiSensor*	m_pNuiSensor;
	HANDLE        m_hNextDepthFrameEvent;
	HANDLE        m_hNextColorFrameEvent;
	HANDLE		  m_hNextSkeletonEvent;
	HANDLE        m_pDepthStreamHandle;
	HANDLE        m_pColorStreamHandle;
	HANDLE        m_hEvents[3];
	NUI_IMAGE_RESOLUTION	m_resDepth, m_resColor;
	NUI_IMAGE_FRAME			m_imageColorFrame, m_imageDepthFrame;
	NUI_SKELETON_FRAME		m_imageSkeletonFrame;

private:	
	bool		m_bIsInit;
	double		m_fFrameScale;
	cv::Size	m_sColor, m_sDepth;
	cv::Size	m_sOrgColor, m_sOrgDepth;
	UserCount m_listUsers;

private:
	SV_TRACKED_SKELETON_MODE	m_svSkelTrackMode;
	DWORD	m_nStickySkeletonIds[2];

private:
	cv::Mat m_mtxDepthValues;
	cv::Mat m_mtxDepthData3D;
	cv::Point m_pJoints2D[NUI_SKELETON_POSITION_COUNT];
	BasicMaths::Vector3 m_pJoints3D[NUI_SKELETON_POSITION_COUNT];
	BasicMaths::Vector3 m_vHead, m_vCShoulder;			// head and center between shoulders 
	BasicMaths::Vector3 m_vLHand, m_vLWrist, m_vLElbow, m_vLShoulder;	// right arm joints
	BasicMaths::Vector3 m_vRHand, m_vRWrist, m_vRElbow, m_vRShoulder;	// left arm joints
	cv::Point m_ptHead, m_ptCShoulder;
	cv::Point m_ptLHand, m_ptLWrist, m_ptLElbow, m_ptLShoulder;	
	cv::Point m_ptRHand, m_ptRWrist, m_ptRElbow, m_ptRShoulder;
	static unsigned int m_nRes[4][2];

private:
	cv::Rect m_rtAlign;

public:
	int			m_nFrmNum;	// index of current frame 
	cv::Mat m_mtxColorImg;	// RGB color image
	int m_numAction;		// number of actions need to be recognized
	double m_corrAction[6];	// correlation coefficients between current action and template actions
	int m_lenFeaKinect;		// length of feature vector
	double m_feaKinect[12];	// feature vector for action recognition

private:
	static double ActionTemplate[6][12];		// action template (mean must be subtracted!!!)
	static double ActionTemplatePowSumRoot[6];		// root of the sum of power value of action templates

	ProtectedClient<imi::WorldFusionServiceClient> * _w_client;

private:
	double FeaMean(double *feaVector, int feaLength);	// mean of feature vector
	void GetActionCorr(void);			// calculate correlation coefficient between current action and action template
	void ShowActionType(int actionIdx);	// show action recognition result
	
	inline HRESULT BlockForResult(ISpRecoContext * pRecoCtxt, ISpRecoResult ** ppResult);
	WCHAR * StopWord();
};
