#This program goal is to have an agent looking at sound sources and recognizing sound classes
#It only uses the sound input, and agent output
import sys
sys.path.append("../../gen-py")
sys.path.append("../../i2p/tools/py")
sys.path.append("../")

import Inputs.SoundService as Sound_Service
import EventPublisher.WorldQuery as WorldQuery_Service
import Control.AgentControl as AgentControl_Service
from Inputs.ttypes import *
from I2P.ttypes import *
import Control.ttypes
from EventPublisher.ttypes import *

import Inputs.constants
import EventPublisher.constants
import Control.constants
import transformations

from random import choice

#to ease client server creation in python
import ThriftTools

from numpy import array
import numpy as np

#time
import I2PTime

#thread
import threading
import time

#the world model
import worldEntity

#cos and sin
import math

#the class listening for sound inputs
class SoundServiceHandler:
    def sound(self, sensorID, timestamp, source):
        theWorld.soundDetected(source)
        
        
#the function updating position to look at
class WorldQueryServiceHandler:
    def __init__(self):
        return
        
    def __del__(self):
        return
        
    def getLocation(self, target):
        targetLocation = EventPublisher.ttypes.Location()
        pos = I2P.ttypes.Vec3()
        pos.x = 0.0
        pos.y = 0.0
        pos.z = 0.0
        quat = I2P.ttypes.Vec4()
        quat.x = 0.0
        quat.y = 0.0
        quat.z = 0.0
        quat.w = 1.0

        try:
            entity = theWorld.worldModel[target]
            entityPosition = entity.getPosition()
            entityOrientation = entity.getOrientation()
            pos.x = entityPosition[0]
            pos.y = entityPosition[1]
            pos.z = entityPosition[2]
            quat.w = entityOrientation[0]
            quat.x = entityOrientation[1]
            quat.y = entityOrientation[2]
            quat.z = entityOrientation[3]
            targetLocation.isUnknown = False
        except KeyError:
            targetLocation.isUnknown = True
            print target,' key error'
        targetLocation.location = pos 
        targetLocation.orientation = quat
        
        return targetLocation

class World(threading.Thread):
    def __init__(self,smartbody_client):
        threading.Thread.__init__(self)
        self.stopping = False
        self.smartbody_client = smartbody_client
        self.lastSpeech = time.time()
        
        # self.worldModel = dict()
        # #initialize the scene
        # display = worldEntity.WorldEntity('VH_Display_01')
        # display.setPosition(array([0.4,1.45,1.8]))
        # display.setOrientation(array([1.0,0.0,0.0,0.0]))
        # self.worldModel['VH_Display_01'] = display
        
        # subject = worldEntity.WorldEntity('Subject')
        # subject.setPosition(array([0.0,0.0,0.0]))
        # subject.setOrientation(array([1.0,0.0,0.0,0.0]))
        # self.worldModel['Subject'] = subject

        
    def run(self):
        while(not self.stopping):
            time.sleep(1)
            
        self.smartbody_client.disconnect()

    def soundDetected(self, source):
        angle = source.azimuth
        soundtype = source.soundtype
        print 'input ',angle,soundtype
        pos = I2P.ttypes.Vec3()
        #in opengl cordinate
        scale = 100.0
        pos.x = scale*2.0*math.sin(angle)
        pos.y = scale*0.0
        pos.z = scale*2.0*math.cos(angle)
        if time.time() - self.lastSpeech > 2.0:
            if soundtype == SoundClass.SPEECH:
                #self.smartbody_client.client.setFaceExpression(I2P.ttypes.Facial_Expression.ANGER,10)
                sentence = choice(['Did you drop something?','I heard something drop','Something dropped there'])                
                #self.smartbody_client.client.speak(sentence,5) 
            elif soundtype == SoundClass.IMPULSIVE_SOUND:
                #self.smartbody_client.client.setFaceExpression(I2P.ttypes.Facial_Expression.SURPRISE,10)
                sentence = choice(['Did you drop something?','I heard something drop','Something dropped there'])
                self.smartbody_client.client.speak(sentence,5)
                #self.smartbody_client.client.lookAtPosition(pos)
            else:
                sentence = choice(['Someones phone is ringing','I hear phone ringing','pick up your phone'])
                self.smartbody_client.client.speak(sentence,5)
            self.lastSpeech = time.time()
            self.smartbody_client.client.lookAtPosition(pos)
        
    def stop(self):
        self.stopping = True

# expect the number of the box to find or 'q' to stop
def readKeyboard1():
    found = False
    # exit condition
    res = -2
    while not found:
        res = raw_input('Enter s to start virtual human client. \
                         Press enter after the key entry.\n')
        if len(res) != 1:
            print ('Wrong option\n')
        else:
            # test exit
            if res[0] == 's':
                res = -1
                found = True
                continue
    return res
    
    
def readKeyboard2():
    found = False
    # exit condition
    res = -2
    while not found:
        res = raw_input('Enter q to exit. \
                         Press enter after the key entry.\n')
        if len(res) != 1:
            print ('Wrong option\n')
        else:
            # test exit
            if res[0] == 'q':
                res = -1
                found = True
                continue
    return res

    
if __name__ == "__main__":        
    
    #starting gesture recognition service
    sound_handler = SoundServiceHandler()
    sound_server = ThriftTools.ThriftServerThread(Inputs.constants.DEFAULT_SOUND_SERVICE_PORT,Sound_Service,\
    sound_handler,'Sound server','155.69.54.83')
    sound_server.start()
    
    #starting worldQuery
    worldQuery_handler = WorldQueryServiceHandler()
    worldQuery_server = ThriftTools.ThriftServerThread(EventPublisher.constants.DEFAULT_WORLD_QUERY_PORT,WorldQuery_Service,worldQuery_handler,'World Query')
    worldQuery_server.start()
    
    res = readKeyboard1()
    while res != -1:
        res = readKeyboard1()
        
    smartbody_client = ThriftTools.ThriftClient('155.69.52.73',9090,AgentControl_Service,'SmartBody')
    smartbody_client.connect()
    theWorld = World(smartbody_client)
    theWorld.start()
    
    servers = [sound_server,worldQuery_server,theWorld]
    
    res = readKeyboard2()
    while res != -1:
        res = readKeyboard2()
    for s in servers:
        s.stop()
    print 'done'