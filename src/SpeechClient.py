import sys, time
sys.path.append("../gen-py")
sys.path.append("../i2p/tools/py")

import I2P
from I2P.ttypes import *
import Inputs.SpeechRecognitionService as SR_Service
from Inputs.ttypes import *
import Inputs.constants
import I2PTime

import I2PTime as i2ptime
import ThriftTools
from thrift.transport import TSocket

import pyaudio
import wave
import subprocess, shlex
import json

class RecordAudio:
    def __init__(self):
        self.CHUNK = 1024
        self.FORMAT = pyaudio.paInt16
        self.CHANNELS = 2
        self.RATE = 44100
        self.RECORD_SECONDS = 5
        self.WAVE_OUTPUT_FILENAME = "output.wav"
        self.audio = pyaudio.PyAudio()
    def record(self):
        print '** Recording **'
        frames = []
        stream = self.audio.open(format=self.FORMAT,
                                 channels=self.CHANNELS,
                                 rate=self.RATE,
                                 input=True,
                                 frames_per_buffer=self.CHUNK)
        for i in range(0, int(self.RATE / self.CHUNK * self.RECORD_SECONDS)):
            data = stream.read(self.CHUNK)
            frames.append(data)
        print "** done recording **"
        stream.stop_stream()
        stream.close()
        # p.terminate()

        wf = wave.open(self.WAVE_OUTPUT_FILENAME, 'wb')
        wf.setnchannels(self.CHANNELS)
        wf.setsampwidth(self.audio.get_sample_size(self.FORMAT))
        wf.setframerate(self.RATE)
        wf.writeframes(b''.join(frames))
        wf.close()

        stream.stop_stream()
        stream.close()
        # p.terminate()

        wf = wave.open(self.WAVE_OUTPUT_FILENAME, 'wb')
        wf.setnchannels(self.CHANNELS)
        wf.setsampwidth(self.audio.get_sample_size(self.FORMAT))
        wf.setframerate(self.RATE)
        wf.writeframes(b''.join(frames))
        wf.close()

    def googleSpeechToText(self):
        # sox command
        commandLine = 'sox output.wav output.flac'
        args = shlex.split(commandLine)
        proc = subprocess.call(args)

        # google wget...
        # dumping output on STDOUT
        # commandLine = 'wget --post-file output.flac --header="Content-Type: audio/x-flac; rate=44100" -O - "http://www.google.com/speech-api/v1/recognize?lang=en-us&client=chromium"'
        # saving result in json file
        commandLine = 'wget --post-file output.flac --header="Content-Type: audio/x-flac; rate=44100" -O text.json "http://www.google.com/speech-api/v1/recognize?lang=en-us&client=chromium"'
        args = shlex.split(commandLine)
        proc = subprocess.call(args)
        googleResult = open('text.json').read()
        retTouple = ('', 0)
        if bool(googleResult):
            returnedDict = json.load(open('text.json'))
            retTouple = (returnedDict['hypotheses'][0]['utterance'],
                         returnedDict['hypotheses'][0]['confidence'])
        return retTouple
        
if __name__ == "__main__":
    sr_client = ThriftTools.ThriftClient('localhost', Inputs.constants.DEFAULT_SOUND_SERVICE_PORT, SR_Service, 'Speech Recognition')
    while True:
        try:
            sr_client.connect()
            break
        except TSocket.TTransportException:
            print 'can not connect to Sound Recognition service: localhost'
            time.sleep(1)
        except:
            break

    audObj = RecordAudio()
    inputKey = ''
    while inputKey != 'q':
        audObj.record()
        result = audObj.googleSpeechToText()
        if result[1] != 0:
            sr_client.client.sentenceRecognized('Microphone',
                                                i2ptime.getTimeStamp(),
                                                result[0],
                                                result[1])
        else:
            print "Speech recognition didn't work"
        print 'press q and return to exit... or return to continue recording'
        inputKey = raw_input()        
