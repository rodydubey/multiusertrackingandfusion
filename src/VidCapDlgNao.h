// VidCapDlgNao.h : header file
//
#ifndef VidCapDlgNao_h_
#define VidCapDlgNao_h_

#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <cv.h>

using namespace cv;
using namespace std;


#define ELLIPSESIZE 6634

#define NUM_OF_REGISTER_USER 7

#define ScanNeighborhood 0.2f
#define CORP_EYE_DISTANCE 70.0f
#define CORP_EYE_T_EDGE 46.0f
#define CORP_EYE_L_EDGE 30.0f

#define ImgHeight 480
#define ImgWidth 640

#define FACE_VERIFY_HEIGHT 150
#define FACE_VERIFY_WIDTH 130

#define FACE_VERIFICATION_THRESHOLD 20.0f


//typedef struct myRect
//{
//    int x;
//    int y;
//    int width;
//    int height;
//}
//myRect;

struct UserInfo
{
  double* verify_face;
  const char* user_name;
  double threshold;
};

struct UserTrackInfo
{
  const char* user_name;
  CvRect* face_rect;
};

#ifndef WIN32
typedef struct tagRECT
{
  int left;
  int top;
  int right;
  int bottom;
} RECT;
#endif

// CVidCapDlg dialog
class CVidCapDlg 
{
  // Construction
 public:
  CVidCapDlg(std::string devicename);	
  ~CVidCapDlg();

 private:
  bool m_bIsRunning;
  unsigned int m_nTimerInterval;
  unsigned int m_nTimer;

  int m_nFramesProcessed;

  CvHaarClassifierCascade* cascade_face;
  CvHaarClassifierCascade* cascade_eye_l;
  CvHaarClassifierCascade* cascade_eye_r;

  bool DetectEyesFromFaceImage(IplImage* pImage,CvHaarClassifierCascade* cascade_eye, CvSize cvMin, CvSize cvMax, RECT** rr);
  bool DetectBothEyeFromFaceImage(IplImage* pImage, IplImage* small_image1, CvRect* rr,CvHaarClassifierCascade* cascade_eye_l,int scale, int LR, RECT** eyeDetect);
		
  double* m_matrix_verify;
  //double* verify_face;
  unsigned char* m_ellipse;
  UserInfo m_WatchList[8];
  UserTrackInfo m_TrackList[2];
  long UCount;

  bool ResetTrackList();
  bool FaceVerification(IplImage* pImage, RECT* eyeRectL, RECT* eyeRectR,int i);
  bool GeoNormal(unsigned char* Image, RECT* eyeRectL, RECT* eyeRectR,unsigned char* CorpFaceImg);
  int round(double floatNumber);
BOOL calHistForWholeImage(BYTE* LBP_image, double* hist);
BOOL calLBPImg(BYTE* Image, BYTE* LBPMap);
int power(int a, int b);
  bool HistEQ(unsigned char* MaskedImg, int ellipseSize);
  bool GrayLelNormal(unsigned char* Image, int ImgSize, double** NorImg);

  long FVCount;

  //bool GetVerificationEllipse(BYTE* ellipse, int height,  int width, double vOffset, double hOffset, double scalefactor);
        
  static int Bpp(cv::Mat img) { return 8 * img.channels(); } 

  std::string m_devicename;
 public:
     
  // void init(string imgName);
  std::string init(cv::Mat nao);
};

#endif VidCapDlgNao_h_
