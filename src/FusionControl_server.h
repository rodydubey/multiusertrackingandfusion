#ifndef FusionControl_server_H
#define FusionControl_server_H

#include <string>
#include "FusionControl.h"
#include "ServerToClient.h"
#include "I2P_types.h"

struct FusionControlServerArguments
{
	ServerToClient* stc;
    FusionControlServerArguments(ServerToClient* ostc)
	:stc(ostc)
    {
    }

};

void startFusionControlServer(FusionControlServerArguments*);

#endif